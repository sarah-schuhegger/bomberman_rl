import numpy as np
import pandas as pd
import sys
import pickle
from random import random
from time import sleep #not yet used
from scipy.linalg import lstsq

from settings import e
from data_handler import DataHandler

sys.path.append("agent_code/cont_agent/")
from q_settings import Settings

def setup(self):
    """
    Setting up or loading Q table (learning or training).
    Maby other yet unknowen definitions
    """
    self.logger.info("Initialisation")
    self.training = False #create or save table
    self.user_training = False #learn by user input
    self.task = 1
    self.dh = DataHandler()
    self.qs = Settings(self.task, self.training)
    self.q_params = self.qs.parameters
    self.acts = self.qs.action_space
    self.q_new = self.qs.q_data
    self.state_new = self.qs.state_data
    self.decision_new = self.qs.decision_data

    # needed reward info 
    self.next_action = ""
    self.last_action = "WAIT"
    self.decision = ""
    self.state = []
    self.states = []
    self.actions = []
    self.decisions = []
    self.rewards = []
    self.q_values = []
    self.max_q_values = []

    self.logger.info("Setup successfull")

def act(self): 
    """
    actual function using the q-table to play the game
    """ 
    self.logger.info("acting")
    p = random()
    self.df = self.dh.game_state_to_df(self.game_state).values.tolist()[0][6:]
    print(self.df)
    self.priorities = act_q(self.df, self.q_params)
    self.logger.info("Priorities")
    self.logger.info(self.priorities)
    if self.user_training:
	# This would have been a possibility to play the game ny one self, but i dont know how to imlement this feature
        #self.logger.info('Pick action according to pressed key')
        #self.next_action = self.game_state['user_input']
        pass
    elif p > self.qs.exploration_rate:
        self.logger.info("act qy") 
        self.decision = np.argmax(self.priorities)
    else:
        self.logger.info("act randomly")
        self.decision = act_random(self.acts) 
    self.next_action = self.acts[self.decision]
    self.logger.info("act next action " + self.next_action)
    self.logger.info("Acted successfully")
    return None

def act_random(acts): 
    """
    Function that will be mostly used during training
    to keep the agent moving while it doesent reach any desicions
    or for exploration while an epsilon greedy strategy
    """
    return np.random.choice([0, 1, 2, 3, 4], p=[.2, .2, .2, .2, .2])
 
def act_q(state, parameters):
    """
    Function to decide based on q functions
    """
    prios = np.dot(parameters, state)
    return prios

def reward_update(self): 
    """
    all the training will happen at the end of the episode, this is only to save the data for said training. This makes it easier to implement n-step learning
    """ 
    self.logger.info("reward update")
    if self.game_state['step'] > 1:
        self.decisions.append(self.decision)
        self.actions.append(self.next_action)
        self.states.append(self.df)
        self.q_values.append(self.priorities[self.decision])
        self.max_q_values.append(max(self.priorities))
        self.rewards.append(self.qs.reward(e, self.events, self.last_action, self.next_action))
        self.last_action = self.next_action
    self.logger.info("rewarded")
    return None


def q_fuction(state, q_values):
    """
    Calculation a value for each possible movement 
    """
    return state 

def update_q(q_old, r, mfr, lr = 0.1, df = 0.9): 
    """
    Updating the q-table according to Bolzmann Formula
    """
    return (1 - lr) * q_old + lr * (r + df * mfr)

def end_of_episode(self):
    self.logger.info("exploration")
    self.logger.info(self.qs.exploration_rate)
    self.logger.info("end of episode")
    if self.qs.exploration_rate > self.qs.min_exploration_rate:
        self.qs.exploration_rate = self.qs.exploration_rate - self.qs.exploration_decay
    for i in range(6):
        mask = (np.array(self.decisions) == i)
        values = np.array(self.q_params[i])
        q_old = np.array(self.q_values)[mask]
        q_max = np.array(self.max_q_values)[mask]
        rew = np.array(self.rewards)[mask]
        states = np.array(self.states)[mask][:-1].tolist()

        up = update_q(q_old[:-1], rew[:-1], q_max[1:], self.qs.learning_rate, self.qs.discount_factor)
        self.logger.info("bugs")
        self.logger.info(self.q_new)
        self.q_new.append(up)
        self.logger.info(self.q_new)
        self.state_new.append(states)
        for n in range(len(up)):
            self.decision_new.append(i)
        totmask = (np.array(self.decision_new) == i)

        self.logger.info("lstsq")
        self.logger.info(self.decision_new)
        self.logger.info(totmask)
        self.logger.info(self.state_new)
        self.logger.info(self.q_new)

        newval, res, rnk, s = lstsq(self.state_new[totmask], self.q_new[totmask])

        self.logger.info("Parameters")
        self.logger.info(self.q_params[i])
        self.logger.info("new")
        self.logger.info(newval)

        self.q_params[i] = newval
    if self.training:
        self.qs.save_q(self.task, self.q_params, self.q_new, self.states, self.decisions)
        self.logger.info("updated parameters saved")
    self.logger.info("end")
    return None