import pandas as pd
import numpy as np
import pickle 


class Settings():

    def __init__(self, task, training):
        # going rom randomly exploring the environment to exploting it
        self.exploration_rate = .9
        self.min_exploration_rate = 0.00
        self.exploration_decay = 0.01

        # values considering the q-learning algorithm as well as handeling different training Methods for different tasks
        self.task = task
        self.training = training
        self.learning_rate = 0.03
        self.discount_factor = 0.9
        self.q_data = []
        self.state_data = []
        self.decision_data = []
        
        # actions and observations
        self.action_space = ['RIGHT', 'LEFT', 'UP', 'DOWN', 'WAIT', 'BOMB']

        # either initialising a the paramenerspace or loading it from previous training runs
        if not self.training:
            self.parameters = np.zeros((6, 38))
            self.saveExt = {"params": self.parameters, 
                         "exploration": self.exploration_rate,
                         "minExp": self.min_exploration_rate,
                         "expDecay": self.exploration_decay,
                         "q_data": self.q_data,
                         "state_data": self.state_data,
                         "decision_data": self.decision_data
                         }
            with open(r"agent_code/cont_agent/q_param/q_task" + str(self.task) + ".pickle", "wb") as file:
                pickle.dump(self.saveExt, file, fix_imports = False)
        else:
            with open(r"agent_code/cont_agent/q_param/q_task" + str(self.task) + ".pickle", "rb") as file:
                self.saveExt = pickle.load(file)
                self.parameters = self.saveExt["params"]
                self.exploration_rate = self.saveExt["exploration"]
                self.min_exploration_rate = self.saveExt["minExp"]
                self.exploration_decay = self.saveExt["expDecay"]
                self.q_data = self.saveExt["q_data"]
                self.state_data = self.saveExt["state_data"]
                self.decision_data = self.saveExt["decision_data"]

    def reward(self, e, events, la, na): 
        """
        A reward funtion for any given action considering coins, deaths of other agents and itseld,
        bombs, blown up crates, ...
        A good base is probably Sarah's target funcion
        """
        rev = 0
        for event in events:
            if event in [e.MOVED_LEFT, e.MOVED_RIGHT, e.MOVED_UP, e.MOVED_DOWN]:
                rev += 1    
            if event == e.WAITED:
                rev -= 1    
            if event == e.INVALID_ACTION:
                rev -= 3
            if event == e.BOMB_DROPPED:
                rev += 0
            if event == e.BOMB_EXPLODED:
                rev += 0
            if event == e.CRATE_DESTROYED:
                rev += 0
            if event == e.COIN_FOUND:
                rev += 20
            if event == e.COIN_COLLECTED:
                rev += 25
            if event == e.KILLED_OPPONENT:
                rev += 0
            if event == e.KILLED_SELF:
                rev -= 25
            if event == e.GOT_KILLED:
                rev += 0
            if event == e.OPPONENT_ELIMINATED:
                rev += 0
            if (la, na) in [("UP", "DOWN"), ("DOWN", "UP"), ("LEFT", "RIGHT"), ("RIGHT", "LEFT")]:
                rev -= 2
        return rev

    def save_q(self, task, parameters, q_data, state_data, decision_data):
        self.saveExt = {"params": parameters, 
                         "exploration": self.exploration_rate,
                         "minExp": self.min_exploration_rate,
                         "expDecay": self.exploration_decay,
                         "q_data": q_data,
                         "state_data": state_data,
                         "decision_data": decision_data
                         }
        with open(r"agent_code/cont_agent/q_param/q_task" + str(self.task) + ".pickle", "wb") as file:
            pickle.dump(self.saveExt, file, fix_imports = False)
        return None