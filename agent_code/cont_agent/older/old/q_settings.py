import itertools as it
import pandas as pd
import numpy as np
import scipy.optimize as opt
import pickle 


class Settings():

    def __init__(self, task, table = False):
        # going rom randomly exploring the environment to exploting it
        self.exploration_rate = 1
        self.min_exploration_rate = 0.001
        self.exploration_decay = 0.001 

        # values considering the q-learning algorithm as well as handeling different training Methods for different tasks
        self.task = task
        self.training = parameters
        self.learning_rate = 0.03
        self.discount_factor = 0.9

        # actions and observations
        self.action_space = ['RIGHT', 'LEFT', 'UP', 'DOWN', 'WAIT', 'BOMB']

        # either initialising a q-table or loading it from previous training runs
        if not self.training:
            self.parameters = np.zeros((6, 100))
            self.saveExt = {"params": self.parameters, 
                         "exploration": self.exploration_rate,
                         "minExp": self.minmin_exploration_rate,
                         "expDecay": self.exploration_decay
                         }
            with open(r"agent_code/cont_agent/q_param/q_task" + str(self.task) + ".pickle", "wb") as file:
                pickle.dump(self.saveExt, file, fix_imports = False)
        else:
            with open(r"agent_code/cont_agent/q_param/q_task" + str(self.task) + ".pickle", "rb") as file:
                self.saveExt = pickle.load(file)
                self.parameters = saveExt["params"]
                self.exploration_rate = saveExt["exploration"]
                self.min_exploration_rate = saveExt["minExp"]
                self.exploration_decay = saveExt["expDecay"]

    def reward(self, e, events, la, na): 
        """
        A reward funtion for any given action considering coins, deaths of other agents and itseld,
        bombs, blown up crates, ...
        A good base is probably Sarah's target funcion
        """
        rev = 0
        for event in events:
            if event in [e.MOVED_LEFT, e.MOVED_RIGHT, e.MOVED_UP, e.MOVED_DOWN]:
                rev -= 1    
            if event == e.WAITED:
                rev -= 1    
            if event == e.INVALID_ACTION:
                rev -= 1
            if event == e.BOMB_DROPPED:
                rev += 0
            if even == e.BOMB_EXPLODED:
                rev += 0
            if event == e.CRATE_DESTROYED:
                rev += 0
            if event == e.COIN_FOUND:
                rev += 0
            if event == e.COIN_COLLECTED:
                rev += 25
            if event == e.KILLED_OPPONENT:
                rev += 0
            if event == e.KILLED_SELF:
                rev -= 25
            if event == e.GOT_KILLED:
                rev += 0
            if event == OPPONENT_ELIMINATED:
                rev += 0
            if (la, na) in [("UP", "DOWN"), ("DOWN", "UP"), ("LEFT", "RIGHT"), ("RIGHT", "LEFT")]:
                rev -= 1
        return rev

    def save_q(self, q, task):
        with open(r"agent_code/q_agent/q_tables/q_task" + str(task) + ".pickle", "wb") as file:
            pickle.dump(q, file, fix_imports = False)
        return None