 import numpy as np
import pandas as pd
import sys
import pickle
from random import random
from time import sleep #not yet used
from scipy.optimize import least_square

from settings import e
from data_handler import DataHandler

sys.path.append("agent_code/q_agent/")
from q_settings import Settings

def setup(self):
    """
    Setting up or loading Q table (learning or training).
    Maby other yet unknowen definitions
    """
    self.logger.info("Initialisation")
    self.training = False #create or save table
    self.user_training = False #learn by user input
    self.task = 1
    self.dh = DataHandler()
    self.qs = Settings(self.task, self.training)
    self.q_params = self.qs.q_params
    self.acts = self.qs.action_space

    # needed reward info 
    self.next_action = ""
    self.decision = ""
    self.state = []
    self.states = []
    self.actions = []
    self.decisions = []
    self.rewards = []

    self.logger.info("Setup successfull")

def act(self): 
    """
    actual function using the q-table to play the game
    """ 
    p = random()
    df = self.dh.game_state_to_df(self.game_state)
    self.current_state = df_to_q(df, self.last_action)
    if self.user_training:
        #self.logger.info('Pick action according to pressed key')
        #self.next_action = self.game_state['user_input']
        pass
    elif p < 1:
        self.logger.info("act") 
        self.decision = np.argmax(self.q_table[self.current_state])
    else:
        self.logger.info("act randomly")
        self.decision = act_random(self.acts) 
    self.next_action = self.acts[self.decision]
    self.logger.info("act next action " + self.next_action)
    self.logger.info("Acted successfully")

def df_to_q(df, la): 
    """
    temporary function to get data from data to useful stuff
    """
    coins = [obs for obs in df.keys() if "COIN" in obs][:4]
    coint = sorted([(a, df[a].values[0]) for a in coins ], key = itemgetter(1), reverse = True)[:2]
    coinst = "".join(map(itemgetter(0), coint))
    dofs = [obs for obs in df.keys() if "DOF" in obs]
    doft = sorted([(a, df[a].values[0]) for a in dofs ], key = itemgetter(1), reverse = True) [:2]
    dofst = "".join(map(itemgetter(0), doft))
    obs = la + coinst + dofst
    return obs

def act_random(acts): 
    """
    Function that will be mostly used during training
    to keep the agent moving while it doesent reach any desicions
    or for exploration while an epsilon greedy strategy
    """
    return np.random.choice(list(range(len(acts))))

def reward_update(self): 
    """
    gets called after every step, dont know what to do yet
    will likely replace the training value
    maybe a combination like 
    """ 
    
    return None

def q_fuction(state, q_values):
    """
    Calculation a value for each possible movement 
    """
    return state 

def update_q(q_old, r, mfr, lr = 0.1, df = 0.9): 
    """
    Updating the q-table according to Bolzmann Formula
    """
    return (1 - lr) * q_old + lr * (r + df * mfr)

def end_of_episode(self):
    if self.training:
        self.qs.save_q(self.q_table, self.task)
        self.logger.info("updated parameters saved")
    return None