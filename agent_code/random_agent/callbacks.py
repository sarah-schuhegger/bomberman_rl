
import numpy as np


def setup(agent):
    np.random.seed()

def act(agent):
    agent.logger.info('Pick action at random')
    agent.next_action = np.random.choice(['RIGHT', 'LEFT', 'UP', 'DOWN', 'BOMB'], p=[.23, .23, .23, .23, .08])
    # agent.next_action = np.random.choice(['RIGHT', 'LEFT', 'UP', 'DOWN'], p=[.25, 0.25, 0.25, 0.25])

def reward_update(agent):
    pass

def end_of_episode(agent):
    pass
