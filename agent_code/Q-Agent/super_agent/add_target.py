import pickle
import numpy as np
import pandas as pd
import copy
import re

import os
import sys
p = os.path.realpath(__file__)
p = os.path.dirname(p)
p = os.path.dirname(p)
p = os.path.dirname(p)
sys.path.append(p)


from agent_code.super_agent.settings_for_ml import (map_weights,
                             comfort_mapping,
                             POTENTIAL,
                             penalization_thinking_to_long,
                             penalization_weights,
                             step_punishment)
from settings import e


class AddTarget():

    def __init__(self):
        pass

    def add_future_rewards(self, df):
        pass

    def add_improvement_target_to_df(self, df, t=5, max_step=400):
        """
        add target.
        -1: agent dies in next t steps
        1: agent levels score up in next t steps
        0: else
        """
        # test if all importent columns are available
        for i in ["MAX_STEP", "CURRENT_SCORE", "WINNER", "ACTIVE_AGENTS", "NAME"]:
            assert i in df.columns, "Column: %s is not in the df, can't add the improvement target." % i
        df["IMPROVEMENT_TARGET_%d" % t] = 0

        # TODO: get max step if winner is the last living agent
        if df[df.STEP == 0].ACTIVE_AGENTS.iloc[0] > 1:
            df_winner_and_last_agent = df[(
                df.WINNER == 1) & (df.ACTIVE_AGENTS == 1)]
            if len(df_winner_and_last_agent) != 0:
                max_step = df_winner_and_last_agent.STEP.iloc[-1]
        for name in np.unique(df.NAME):
            df_agent = df[df.NAME == name]
            # add -1

            personal_max_step = df_agent.MAX_STEP.iloc[0]
            # if agent died
            if personal_max_step < max_step:
                df.loc[(df_agent["STEP"] >= personal_max_step - t) &
                       (df.NAME == name), "IMPROVEMENT_TARGET_%d" % t] = -1
            # add 1 for score improvement
            df_agent.loc[:, "SCORE_IN_%d_STEPS" % t] = np.array(
                list(df_agent["CURRENT_SCORE"][t:]) + [df_agent.CURRENT_SCORE.iloc[-1]]*t).copy()
            df.loc[(df_agent["SCORE_IN_%d_STEPS" % t] > df_agent["CURRENT_SCORE"]) & (
                df.NAME == name), "IMPROVEMENT_TARGET_%d" % t] = 1

        return df

    def future_rewards(self, df, discount_factor=0.795):
        """
        Add future rewards to the dataframe. 
        Choose default discount factor, so that after 20 steps the gain has a weight of about 1%
        (0.01)**(1/20) = 0.795.
        Add Further binary Feature "CUT_OFF", that feature tells you, if you are at the last m steps of the game, 
        where the weight for the step is around 1 %. 
        For default value: m=20 -> all steps until max_step-m are 0 after that they are 1.
        """
        df["FUTURE_REWORDS_DISCOUNT%1.3f" % discount_factor] = 0
        assert len(set(["NAME", "MAX_STEP"]) - set(df.columns)) == 0, \
            "can't find name of agents in dataframe under column NAME"
        discount_rate = (1-discount_factor)/discount_factor
        max_step_game = max(df.MAX_STEP)
        cut_off = int((np.log(0.01)/np.log(discount_factor)) + 0.5)
        for agent in df.NAME.unique():
            max_step = df[df.NAME == agent].MAX_STEP.iloc[0]
            last_event = df[df.NAME == agent].LAST_EVENT.iloc[0]
            scores = np.array(df[df.NAME == agent].CURRENT_SCORE)
            fr = []
            last_event_weight = penalization_weights[last_event]
            for i in range(max_step):
                diff = list(scores[i+1:]-scores[i:-1]) + [last_event_weight]
                fr.append(np.npv(discount_rate, diff))
            df.loc[(df.NAME == agent), "FUTURE_REWORDS_DISCOUNT%1.3f" %
                   discount_factor] = fr + [0]
        # include CUT_OFF
        df["CUT_OFF_FUTURE_REWARDS%1.3f" % discount_factor] = 0
        df.loc[
            (df.MAX_STEP == max_step_game) & (df.STEP >=
                                              (max_step_game - cut_off)) & (df.LAST_EVENT == e.SURVIVED_ROUND),
            "CUT_OFF_FUTURE_REWARDS%1.3f" % discount_factor] = 1
        return df

    # TODO: change discount factor and -1 per round
    def add_task1_target(self, df, discount_factor=0.631, step_punishment=step_punishment):
        """
        Add future rewards to the dataframe. 
        Choose discount value, that after 10 steps, the has a weight of 1% (to minimize the variance), 
        because a agent does not have to look: (0.01)**(1/10) = 0.631
        Further more we want to subract 0.05 points per step to encourage the agent for efficient collecting.
        """
        df["TARGET_TASK1"] = 0
        assert len(set(["NAME", "MAX_STEP"]) - set(df.columns)) == 0, \
            "can't find name of agents in dataframe under column NAME"
        discount_rate = (1-discount_factor)/discount_factor
        max_step_game = max(df.MAX_STEP)
        cut_off = int((np.log(0.01)/np.log(discount_factor)) + 0.5)
        for agent in df.NAME.unique():
            max_step = df[df.NAME == agent].MAX_STEP.iloc[0]
            last_event = df[df.NAME == agent].LAST_EVENT.iloc[0]
            scores = np.array(df[df.NAME == agent].CURRENT_SCORE)
            fr = []
            last_event_weight = penalization_weights[last_event]
            for i in range(max_step):
                diff = list(scores[i+1:]-scores[i:-1]) + [last_event_weight]
                diff = np.array(diff) - step_punishment
                fr.append(np.npv(discount_rate, diff))
            df.loc[(df.NAME == agent), "TARGET_TASK1"] = fr + [0]
        # include CUT_OFF
        df["CUT_OFF_TARGET_TASK1"] = 0
        df.loc[
            (df.MAX_STEP == max_step_game) & (df.STEP >=
                                              (max_step_game - cut_off)) & (df.LAST_EVENT == e.SURVIVED_ROUND),
            "CUT_OFF_TARGET_TASK1"] = 1
        return df

    def add_task2_target(self, df, discount_factor=0.794, step_punishment=step_punishment):
        """
        Add future rewards to the dataframe. 
        Choose discount value, that after 20 steps, the has a weight of 1% (to minimize the variance), 
        because a agent does not have to look: (0.01)**(1/20) = 0.736
        Further more we want to subract 0.05 points per step to encourage the agent for efficient collecting.
        """
        df["TARGET_TASK2"] = 0
        assert len(set(["NAME", "MAX_STEP"]) - set(df.columns)) == 0, \
            "can't find name of agents in dataframe under column NAME"
        discount_rate = (1-discount_factor)/discount_factor
        max_step_game = max(df.MAX_STEP)
        cut_off = int((np.log(0.01)/np.log(discount_factor)) + 0.5)
        for agent in df.NAME.unique():
            max_step = df[df.NAME == agent].MAX_STEP.iloc[0]
            last_event = df[df.NAME == agent].LAST_EVENT.iloc[0]

            scores = np.array(df[df.NAME == agent].CURRENT_SCORE)
            fr = []
            last_event_weight = penalization_weights[last_event]
            for i in range(max_step):
                diff = list(scores[i+1:]-scores[i:-1]) + [last_event_weight]
                # for task 2 we do not want to have a step punishment for each step
                diff = np.array(diff)
                fr.append(np.npv(discount_rate, diff))
            df.loc[(df.NAME == agent), "TARGET_TASK2"] = fr + [0]
        # include CUT_OFF
        df["CUT_OFF_TARGET_TASK2"] = 0
        df.loc[
            (df.MAX_STEP == max_step_game) & (df.STEP >=
                                              (max_step_game - cut_off)) & (df.LAST_EVENT == e.SURVIVED_ROUND),
            "CUT_OFF_TARGET_TASK2"] = 1
        # cut off last step, if agent killed himself, otherwise the target task2 would be 0
        df.loc[
            (df.LAST_EVENT == e.KILLED_SELF) & (df.STEP == df.MAX_STEP),
            "CUT_OFF_TARGET_TASK2"] = 1

        return df

    def survive_next_steps(self, df, steps=4):
        """
        Add feature 0/1, if you survive the next m steps
        Add CUT_OFF_SURVIVE feature (0/1) it is 1 for the agent which survives the longest period of time 
        at the last max_step - steps values. 
        default steps is 4, because:
        You need 4 steps to flee from a bomb, if you have a straight street.
        The agent should look at least 4 steps into the future"""

        # Add Survive Feature
        df["SURVIVE_%dSTEPS" % steps] = 1
        df.loc[df.STEP > (df.MAX_STEP - steps), "SURVIVE_%dSTEPS" % steps] = 0

        # Add Survive Cut off
        max_step_game = max(df.MAX_STEP)
        df["CUT_OFF_SURVIVE%d" % steps] = 0
        df.loc[(df.MAX_STEP == max_step_game) & (df.STEP > (
            max_step_game - steps)), "CUT_OFF_SURVIVE%d" % steps] = 1
        return df

