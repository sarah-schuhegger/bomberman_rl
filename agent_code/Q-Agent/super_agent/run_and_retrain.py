import pickle
import datetime
import numpy as np
import pandas as pd
import copy
import re
import os
from os import walk
from time import time
import logging
import sys

p = os.path.realpath(__file__)
p = os.path.dirname(p)
p = os.path.dirname(p)
p = os.path.dirname(p)
sys.path.append(p)

from settings import e
from settings import settings as s
from main import main

from agent_code.super_agent.settings_for_ml import (map_weights, 
                                                    comfort_mapping, 
                                                    prediction_dict, 
                                                    step_punishment, 
                                                    max_depth)
from agent_code.super_agent.prediction import Prediction
from agent_code.super_agent.data_merger import DataMerger


logging.basicConfig(level=logging.INFO, format='%(asctime)s:%(name)s:%(levelname)s: %(message)s')
logger = logging.getLogger('std')
handler = logging.FileHandler("run_and_retrain.log", mode="w")
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(name)s] %(levelname)s: %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

"""
task = "task1"
task1_to_drop = prediction_dict[task]["to_drop"]
target = prediction_dict[task]["target"]
cut_off = prediction_dict[task]["cut_off"]
base_data_name = prediction_dict[task]["prediction_base_data_name"]
prediciton_path = prediction_dict[task]["prediction_path"]
index = prediction_dict[task]["index"]
"""

class Run(): 

    def __init__(self, dir_name, iterations, step_punishment=step_punishment , task="task1", **kwargs): 
        s["gui"] == False, "GUI is True."

        self.task=task
        self.dir_name = dir_name
        self.iterations = iterations
        self.dm = DataMerger()
        self.step_punishment = step_punishment
        # get current date and time
        self.time = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M")
        self.path_temp = "%s/time_of_run_%s" % (self.dir_name, self.time)
    
        if not os.path.exists(self.path_temp):
            os.makedirs(self.path_temp)
        self.target = kwargs["target"]
        self.cut_off = kwargs["cut_off"]
        self.index = kwargs["index"]
        # track scores: 
        self.scores = []
        self.scores_std = []
        self.modified_scores = []
        self.modified_scores_std = []
        self.steps_survived = []
        self.steps_survived_std = []

        #self.base_data_name = kwargs["base_data_name"]
        #self.base_data_path = kwargs["base_data_path"]

        self.base_prediction_name = kwargs["base_prediction_name"]
        self.base_prediction_path = kwargs["base_prediction_path"]
        self.base_prediction_data_name = kwargs["base_prediction_data_name"]
        self.to_drop = kwargs["to_drop"]

        self.new_prediction_data_name = self.time + "_" + self.base_prediction_data_name
        self.new_prediction_name = self.time + "_" + self.base_prediction_name
        self.new_prediction_data_path = self.base_prediction_path +  self.new_prediction_data_name
        self.new_prediction_path = self.base_prediction_path +  self.new_prediction_name
        # load base data 
        self.base_prediction_data_path = self.base_prediction_path + self.base_prediction_data_name
        if os.path.exists(self.base_prediction_data_path):
            print("load %s" % self.base_prediction_data_path)
            self.base_pred_data = pd.read_csv(self.base_prediction_data_path, index_col=0)
        else: 
            raise ValueError("no start data found: %s" %self.base_prediction_data_path)

        print(">> save new prediction under path: %s" % self.new_prediction_path)

    def run(self):
        """
        - run and retrain data 
        - create for each episode a new directory
        - merge all dataframes always to one dataframe 
        - train on over all dataframe again 
        - run trained agents and get new data 
        save prediction under: self.new_prediction_path 
        save data under: self.new_prediction_data_path
        """ 
        
        # create directory if it does not exist

        file_length = len(os.listdir(self.path_temp))
        pygame_init = True
        logger.debug(">> shape of initial training data: ({}, {})".format(self.base_pred_data.shape[0], self.base_pred_data.shape[1]))
        for i in range(self.iterations):
            t0 = time()
            logger.info(">> round: {}".format(i + 1))
            if self.task == "task1":
                logger.info(">> create data for task: run one super agent")
                main(dir_name = self.path_temp, super_agent_count=1, simple_agent_count=0, random_agent_count=0, pygame_init=pygame_init)
                pygame_init = False
                logger.info(">> time to create the new data: {:6.2f} s".format(time()-t0))
            elif self.task == "task2":
                logger.info(">> create data for task 2: run one super agent 30 times")
                main(dir_name = self.path_temp, super_agent_count=1, simple_agent_count=0, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 10/30")
                main(dir_name = self.path_temp, super_agent_count=1, simple_agent_count=0, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 20/30")
                main(dir_name = self.path_temp, super_agent_count=1, simple_agent_count=0, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 30/30")
                logger.info(">> Run one simple agent 40 times")
                main(dir_name = self.path_temp, super_agent_count=0, simple_agent_count=1, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 10/40")
                main(dir_name = self.path_temp, super_agent_count=0, simple_agent_count=1, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 20/40")
                main(dir_name = self.path_temp, super_agent_count=0, simple_agent_count=1, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 30/40")
                main(dir_name = self.path_temp, super_agent_count=0, simple_agent_count=1, random_agent_count=0, pygame_init=pygame_init)
                logger.info(">> 40/40")


                pygame_init = False
                logger.info(">> time to create the new data: {:6.2f} s".format(time()-t0))
            file_list = ["%s/%s" % (self.path_temp, i) for i in os.listdir(self.path_temp)[(file_length):]]
            logger.info(">> lenght of file list: {}".format(len(file_list)))
            # create temporary base data
            df_base_temp = self.dm.create_base_data_from_files(file_list, save=False)

            # calculate scores 
            mean_score, std_score, mean_mod_score, std_mod_score, mean_steps, std_steps = self.get_scores(df_base_temp, step_punishment=step_punishment)
            self.scores.append(mean_score)
            self.scores_std.append(std_score)
            self.modified_scores.append(mean_mod_score)
            self.modified_scores_std.append(std_mod_score)
            self.steps_survived.append(mean_steps)
            self.steps_survived_std.append(std_steps)
            logger.info(">> score: {:6.2f} +- {:6.2f} \t modified score: {:6.2f} +- {:6.2f}  \t steps survived: {:6.2f} +- {:6.2f} ".format(
                mean_score, std_score, mean_mod_score, std_mod_score, mean_steps, std_steps))
            
            # create data for prediction from temporary base data
            df_pred_temp = self.dm.prepare_p_data(self.target, 
                                            self.cut_off, 
                                            to_drop=self.to_drop, 
                                            df=df_base_temp,
                                            base_data_path=False,
                                            prediction_id=False, 
                                            return_df=True,
                                            save=False) 
            # concatenate prediction data with previous data
            self.base_pred_data = pd.concat([self.base_pred_data, df_pred_temp], axis=0, sort=False)
    
            logger.info(">> shape of base_pred_data: ({:6.1f}, {:6.1f}) ".format(self.base_pred_data.shape[0], self.base_pred_data.shape[1]))
            # define new file_length
            file_length = len(os.listdir(self.path_temp))
            # create the new prediction
            self.prepare_predictions(self.base_pred_data, target=self.target, to_drop=self.index)
            # rename the prediction name, which should be used for 
            prediction_dict[self.task]["prediction_name"] = self.new_prediction_name
            logger.info(">> save name of prediction: {} into the prediction-dictionary".format(prediction_dict[self.task]["prediction_name"]))
            del [df_base_temp]
    
        logger.info(">> save data for prediction under: {}".format(self.new_prediction_data_name))
        self.base_pred_data.to_csv(self.new_prediction_data_path)


    def get_scores(self, df_base_temp, step_punishment=step_punishment):
        """
        calculate the mean score and std for an iteration of games
        """
        scores = []
        modified_scores = []
        steps_survived = []
        for i in np.unique(df_base_temp.GAME):
            if df_base_temp[(df_base_temp.GAME == i)].NAME.iloc[0] == "super_agent":
                steps_survived.append(len(df_base_temp[(df_base_temp.GAME == i)]))
                scores.append(max(df_base_temp[(df_base_temp.GAME == i)].CURRENT_SCORE))
                modified_scores.append(max(df_base_temp[df_base_temp.GAME == i].CURRENT_SCORE) - step_punishment*len(df_base_temp[df_base_temp.GAME == i]))
        return np.mean(scores), np.std(scores), np.mean(modified_scores), np.std(modified_scores), np.mean(steps_survived), np.std(steps_survived)
    
    def prepare_predictions(self, df, target, to_drop, max_depth=max_depth): 
        # prepare the prediction data
        print(">> prepare data for prediction.")


        # get p2 and p3 prediction
        print(">> create prediction")
        p = Prediction(df, 
                        classifier_name="RandomForestRegressor", 
                        target=target, 
                        to_drop=to_drop, 
                        max_depth=max_depth,
                        )

        p.save_model(name=self.new_prediction_path)
        logger.info(">> Save new prediciton under path: {}".format(self.new_prediction_path))
        return df

if __name__ == "__main__": 
    simple_run = Run("training_data_task2_simple_random_base", **prediction_dict["task2"], task="task2", iterations=10)
    simple_run.run()
    print("scores: ", simple_run.scores, simple_run.scores_std)
    print("modified scores: ", simple_run.modified_scores, simple_run.modified_scores_std)
    print("steps survive: ", simple_run.steps_survived, simple_run.steps_survived_std)