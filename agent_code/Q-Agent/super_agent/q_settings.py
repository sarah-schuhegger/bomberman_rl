import pandas as pd
import numpy as np
import pickle 
from operator import itemgetter

class Settings():

    def __init__(self, task, training):
        # going rom randomly exploring the environment to exploting it
        self.exploration_rate = .9
        self.min_exploration_rate = 0.08
        self.exploration_decay = 0.004

        # values considering the q-learning algorithm as well as handeling different training Methods for different tasks
        self.task = task
        self.training = training
        self.learning_rate = 0.03
        self.discount_factor = 0.9
        self.cut = 8000
        self.q_data = []
        self.state_data = []
        self.decision_data = []
        
        # actions and observations
        self.action_space = ['RIGHT', 'LEFT', 'UP', 'DOWN', 'WAIT', 'BOMB']

        # either initialising a the paramenerspace or loading it from previous training runs
        if not self.training:
            self.parameters = np.zeros((6, 55))
            self.saveExt = {"params": self.parameters, 
                         "exploration": self.exploration_rate,
                         "q_data": self.q_data,
                         "state_data": self.state_data,
                         "decision_data": self.decision_data
                         }
            with open(r"q_param/q_task" + str(self.task) + ".pickle", "wb") as file:
                pickle.dump(self.saveExt, file, fix_imports = False)
        else:
            with open(r"q_param/q_task" + str(self.task) + ".pickle", "rb") as file:
                self.saveExt = pickle.load(file)
                self.parameters = self.saveExt["params"]
                self.exploration_rate = self.saveExt["exploration"]
                self.q_data = self.saveExt["q_data"]
                self.state_data = self.saveExt["state_data"]
                self.decision_data = self.saveExt["decision_data"]
                
    def reward_values(self, df):
        """
        Calculation of values or parameters needed for the reward function
        """
        CoinList = [obs for obs in df.keys() if "COIN_POT" in obs][:4]
        CoinSort = sorted([(a, df[a].values[0]) for a in CoinList], key = itemgetter(1), reverse = True)[:2]
        MaxCoin = CoinSort[0][0]
        SecCoin = CoinSort[1][0]

        crates = df["CRATES_TO_DESTROY_CENTER"][0]

        DE = []
        endList = [obs for obs in df.keys() if "DEAD_END" in obs]
        for name in endList:
            if df[name][0] == 1:
                DE.append(name)

        explosionContact = []
        explosionList = [obs for obs in df.keys() if "CONTACT_TO_EXPLOSION" in obs]
        for name in explosionList:
            if df[name][0] == 1:
                explosionContact.append(name)

        return MaxCoin, SecCoin, crates, DE, explosionContact

    def reward(self, e, events, la, na, distcoin, lastcoin, bomb, df): 
        """
        A reward funtion for any given action considering coins, deaths of other agents and itseld,
        bombs, blown up crates, ...
        A good base is probably Sarah's target funcion
        """
        rev = -2
        # Additional reward values
        maxCoin, secCoin, crates, DE, explosionContact = self.reward_values(df)
        print(maxCoin, secCoin, crates, DE, explosionContact)
        # Additionally added rewards
        if (la, na) in [("UP", "DOWN"), ("DOWN", "UP"), ("LEFT", "RIGHT"), ("RIGHT", "LEFT")]:
            rev -= 4

        if bomb == 0 and e.BOMB_DROPPED in events :
            rev -= 3
        elif bomb == 1 and e.BOMB_DROPPED in events:
            rev += 2  * crates

        if la == "BOMB":
            temp = 0
            for end in DE:
                if na in end:
                    temp -= 4
            if e.WAITED in events:
                temp -= 2
            if temp == 0:
                temp += 3
            rev += temp

        if lastcoin > distcoin:
            rev += 0
        else:
            rev -= 0

        for contact in explosionContact:
            if na in contact:
                rev -= 5

        # rewards for events happend
        for event in events:
            if event in [e.MOVED_LEFT, e.MOVED_RIGHT, e.MOVED_UP, e.MOVED_DOWN]:
                rev += 2
                if na in maxCoin:
                    rev += 6
                if na in secCoin:
                    rev += 4

            if event == e.WAITED:
                rev -= 5
                if "CONTACT_TO_EXPLOSION_CENTER" in explosionContact:
                    rev -= 0

            if event == e.INVALID_ACTION:
                rev -= 4

            if event == e.BOMB_DROPPED:
                rev -= 2

            if event == e.BOMB_EXPLODED:
                rev += 0

            if event == e.CRATE_DESTROYED:
                rev += 0

            if event == e.COIN_FOUND:
                rev += 0

            if event == e.COIN_COLLECTED:
                rev += 15

            if event == e.KILLED_OPPONENT:
                rev += 0

            if event == e.KILLED_SELF:
                rev -= 0

            if event == e.GOT_KILLED:
                rev -= 0

            if event == e.OPPONENT_ELIMINATED:
                rev += 0
                
        return rev

    def save_q(self, task, parameters, q_data, state_data, decision_data):
        self.saveExt = {"params": parameters, 
                         "exploration": self.exploration_rate,
                         "q_data": q_data,
                         "state_data": state_data,
                         "decision_data": decision_data
                         }
        with open(r"q_param/q_task" + str(self.task) + ".pickle", "wb") as file:
            pickle.dump(self.saveExt, file, fix_imports = False)
        return None