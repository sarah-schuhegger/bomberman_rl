import pickle
import numpy as np
import pandas as pd
import re
import os
import os
import sys
p = os.path.realpath(__file__)
p = os.path.dirname(p)
p = os.path.dirname(p)
p = os.path.dirname(p)
sys.path.append(p)

from agent_code.super_agent.settings_for_ml import map_weights, comfort_mapping, POTENTIAL
from settings import e


class Potential():

    def __init__(self, p=p):
        self.previous_coins = []

        p = os.path.realpath(__file__)
        p = os.path.dirname(p)
        print("load: ", p)
        p = os.path.join(p, POTENTIAL)
        self.potential_ref = np.load(p)
        # self.potential_ref = np.load(POTENTIAL)

    def generate_potential_ref(self, max_val=29, multiplier=1):
        length = max_val*2+1
        cmap = np.zeros((length, length))
        for y in range(length):
            for x in range(length):
                if np.abs(y-max_val)+np.abs(x-max_val) <= max_val:
                    cmap[x][y] = 1/((np.abs(y-max_val)+np.abs(x-max_val))+1)
        return cmap*multiplier

    def initialize_potential_playground(self):
        return np.zeros((17, 17))

    def save_potential_ref(self, cmap, overwrite=False):
        multiplier = cmap[int(len((cmap)-1)/2)][int(len((cmap)-1)/2)]
        print(multiplier)
        name = "../coin_potential_%d-%d-%d.npy" % (
            cmap.shape[0], cmap.shape[1], multiplier)
        if (name == "../" + POTENTIAL) and (overwrite == False):
            raise NameError(
                'Set overwrite to True to overwrite the existing file.')
        np.save(name, cmap)
        print("save coin potential reference matrix under %s" % name)

    def object_to_potential(self, x, y, potential_playground, subtract=False):
        """
        add the potential of a coin to your coin potential matrix. 
        potential_ref: matrix, reference for potential of one coin
        potential_playground: matrix, playground where all potentials of the coins are included 
        """
        center = int((len(self.potential_ref)-1)/2)
        playground_width = len(potential_playground)
        # define cut-off for potential_ref
        x_from = center-x
        x_to = x_from + playground_width
        y_from = center-y
        y_to = y_from + playground_width
        if subtract:
            potential_playground = potential_playground - \
                self.potential_ref[y_from:y_to, x_from:x_to]
        else:
            potential_playground = potential_playground + \
                self.potential_ref[y_from:y_to, x_from:x_to]

        return potential_playground

    def array_to_potential(self, array, potential_3d):
        """
        add array to potential. For example coordinates of an other player
        """

        for step, coo in enumerate(array):
            potential_3d[:, :, step] = self.object_to_potential(
                coo[0], coo[1], potential_3d[:, :, step])
        return potential_3d

    def list_of_xys_to_3d_matrix(self, player_list, len_game):
        """
        example for player_list: [d["%s_xy" % a] for a in d["agents"] if a != my_agent]
        len_game = len(d["bombs])
        """
        pp = self.array_to_potential(player_list[0], np.full(
            (17, 17, len_game), 0, dtype=float))
        for player in player_list[1:]:
            pp = self.array_to_potential(player, pp)
        return pp

    def get_potential_3d_matrix(self, coin_list):
        """
        generate for each step the coin-potential matrix and 
        write it into a 3d matrix
        """
        cpotential_3d = np.full((17, 17, len(coin_list)), 0, dtype=float)
        potential_playground = self.initialize_potential_playground()
        for step, coins in enumerate(coin_list):
            if coins != self.previous_coins or len(coin_list) == 1:
                # set previous coins to new coin
                self.previous_coins = coins
                # set coin potential to zero
                potential_playground = self.initialize_potential_playground()
                potential_playground = self.objects_to_potential(
                    coins, potential_playground)
                # compute new coin potential matrix
                cpotential_3d[:, :, step] = potential_playground
            else:
                cpotential_3d[:, :, step] = potential_playground
        return cpotential_3d

    def objects_to_potential(self, coin_list, potential_playground, subtract=False):
        """
        coin_list, list of available coins for each step
        """
        for coin in coin_list:
            potential_playground = self.object_to_potential(
                coin[0], coin[1], potential_playground, subtract=subtract)
        return potential_playground

    def get_potential_features(self, x, y, potential_playground):
        """
        get the potential for each direction
        """
        cpotential = {"LEFT": 0, "UP": 0, "RIGHT": 0, "DOWN": 0}
        for direction, dx, dy in zip(["LEFT", "UP", "RIGHT", "DOWN"],
                                     [-1, 0, 1, 0],
                                     [0, -1, 0, 1]):
            cpotential[direction] = round(potential_playground[y+dy][x+dx], 3)
        return cpotential

    def get_potential_maxfeatures(self, x, y, potential_playground):
        """
        get the maximum potential in one step 
        """
        directions = ["LEFT", "UP", "RIGHT", "DOWN"]
        cpotential_bool = {"LEFT": 0, "UP": 0, "RIGHT": 0, "DOWN": 0}
        cpotentials = np.array([potential_playground[y][x-1],
                                potential_playground[y-1][x],
                                potential_playground[y][x+1],
                                potential_playground[y+1][x]])
        # test that not all potentials are 0 -> no maximum direction
        if sum(cpotentials) != 0:
            max_dir = directions[np.argmax(cpotentials)]
            cpotential_bool[max_dir] = 1
        return cpotential_bool
