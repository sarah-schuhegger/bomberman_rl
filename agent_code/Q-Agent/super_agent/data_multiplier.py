import pickle
import numpy as np
import pandas as pd
import re
import os
import sys
p = os.path.realpath(__file__)
p = os.path.dirname(p)
p = os.path.dirname(p)
p = os.path.dirname(p)
sys.path.append(p)

from agent_code.super_agent.settings_for_ml import map_weights, comfort_mapping
from settings import e



class DataMultiplier():

    def __init__(self): 
        pass

    def eightfold_data(self, df): 
        """
        Increase data eightfold by rotating and reflecting the data
        """
        # reflect data
        df_ref = self.reflect_data(df)
        # rotate original data df 3-times
        df_rotate = self.rotate_data(df)
        # rotate reflected data df_ref 3-times
        df_ref_rotate = self.rotate_data(df_ref)
        # merge all dataframes
        df_all = pd.concat([df_rotate, df_ref_rotate], axis=0, ignore_index=True, sort=False)
        # test length of output dataframe
        assert len(df_all) == 8*len(df), "Output dataframe has not 8-times the lenght of the input data" 
        return df_all

    def rotate_data(self, df):
        """ 
        Rotate data df 3-times, Output df has length 4*len(df)
        """
        rotate = {"LEFT": "UP", "UP": "RIGHT", "RIGHT": "DOWN", "DOWN": "LEFT"}
        direction_cols = [i for i in df.columns if i.endswith(("LEFT", "UP", "RIGHT", "DOWN"))]
        # print all columns which will not be rotatet
        known_not_rot_cols = ["ACTIVE_AGENTS", "STEP", "X", "Y", 
                                                "DANGER_LEVEL_CENTER", "NEXT_EVENT_WAIT",
                                                "NEXT_EVENT_BOMB", "CRATES_TO_DESTROY_CENTER"
                                                ]
        print("The Following Columns will not be rotated: ", set(df.columns)-set(direction_cols)- set(known_not_rot_cols))
        pattern = re.compile("(LEFT|RIGHT|UP|DOWN)", re.I)
        #print(set(df.columns)-set(direction_cols))
        # rotate data 3 times
        df_all = df.copy()
        for i in range(3): 
            df_ = df.copy()
            for col in direction_cols: 
                direction = pattern.search(col).group(0)
                col_mapped = "%s%s" % (pattern.sub("", col), rotate[direction])
                df_[col_mapped] = df[col]
            # define new previous data-set
            df = df_.copy()
            # concat rotated data
            df_all = pd.concat([df_all, df_], axis=0, ignore_index=True, sort=False)
        # Test length of data
        assert len(df_all) == 4*len(df), "Output dataframe has not 4-times the lenght of the input data" 
        return df_all

    def reflect_data(self, df): 
        """
        Reflect data: 
        Right -> Left
        Left -> Right
        """
        known_not_rot_cols = ["ACTIVE_AGENTS", "STEP", "X", "Y", 
                                                "DANGER_LEVEL_CENTER", "NEXT_EVENT_WAIT",
                                                "NEXT_EVENT_BOMB", "CRATES_TO_DESTROY_CENTER"
                                                ]
        reflect = {"RIGHT": "LEFT", "LEFT": "RIGHT"}
        direction_cols = [i for i in df.columns if i.endswith(tuple(reflect.keys()))]
        print("The following columns will not be reflected: ", set(df.columns)- set(direction_cols) - set(known_not_rot_cols))
        pattern = re.compile("(LEFT|RIGHT)", re.I)
        df_ref = df.copy()

        for col in direction_cols: 
            direction = pattern.search(col).group(0)
            col_mapped = "%s%s" % (pattern.sub("", col), reflect[direction])
            df_ref[col_mapped] = df[col]
        return df_ref