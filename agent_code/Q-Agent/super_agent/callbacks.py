import numpy as np
import pandas as pd
import sys
import os
import pickle
from random import random
from scipy.optimize import lsq_linear
from settings import e
p = os.path.realpath(__file__)
p = os.path.dirname(p)
p = os.path.dirname(p)
p = os.path.dirname(p)
sys.path.append(p)
from agent_code.super_agent.data_handler import DataHandler
from agent_code.super_agent.q_settings import Settings

def setup(self):
    """
    Setting up or loading Q parameters (learning or training).
    Defining all needed variables
    """
    self.logger.info("Initialisation")
    self.training = 1 #create or save table
    self.user_training = False #learn by user input
    self.task = 1
    self.dh = DataHandler()
    self.qs = Settings(self.task, self.training)
    self.q_params = self.qs.parameters
    self.acts = self.qs.action_space
    self.q_new = self.qs.q_data
    self.state_new = self.qs.state_data
    self.decision_new = self.qs.decision_data

    # needed reward info (step)
    self.dist_to_coin = 1
    self.last_to_coin = 1

    # needed reward info (endround)
    self.next_action = ""
    self.last_action = "WAIT"
    self.decision = ""
    self.state = []
    self.states = []
    self.actions = []
    self.decisions = []
    self.rewards = []
    self.q_values = []
    self.max_q_values = []

    self.logger.info("Setup successfull")

def act(self): 
    """
    actual function using the q-table to play the game
    """ 
    self.logger.info("acting")
    # random value that decideds weather to act randomly or decide by the q agent
    p = random()
    
    # loading the data frame for the q agent
    # defining variables for the reward
    df2 = self.dh.game_state_to_df(self.game_state)
    self.df1 = self.dh.previous_event_to_df(df2, self.last_action, self.acts)
    self.dist_to_coin = self.df1['CLOSEST_DISTANCE_COIN'][0]
    
    #features for Q-Learning
    self.df = self.df1.values.tolist()[0][5:]
    self.df.append((self.game_state["self"][3] + 1) % 2)
    
    # Q values
    self.priorities = act_q(self.df, self.q_params)

    self.logger.info("state")
    self.logger.info(self.df)
    self.logger.info("Priorities")
    print(self.priorities)
    self.logger.info(self.priorities)
    
    if self.user_training:
	    # This would have been a possibility to play the game ny one self, but i dont know how to imlement this feature
        #self.logger.info('Pick action according to pressed key')
        #self.next_action = self.game_state['user_input']
        pass
    elif p > self.qs.exploration_rate:
        self.logger.info("act qy") 
        self.decision = np.argmax(self.priorities)
    else:
        self.logger.info("act randomly")
        self.decision = act_random(self.acts) 
    self.next_action = self.acts[self.decision]
    self.logger.info("act next action " + self.next_action)
    self.logger.info("Acted successfully")
    print(self.next_action)
    return None

def act_random(acts): 
    """
    Function that will be mostly used during training
    to keep the agent moving while it doesn't reach any desicions
    or for exploration while an epsilon greedy strategy
    """
    return np.random.choice([0, 1, 2, 3, 4, 5], p=[.2, .2, .2, .2, .1, .1])
 
def act_q(state, parameters):
    """
    Function to decide based on q functions
    """
    prios = np.dot(parameters, state)
    return prios

def reward_update(self): 
    """
    all the training will happen at the end of the episode, this is only to save the data for said training. This makes it easier to implement n-step learning
    """ 
    self.logger.info("reward update")
    if self.game_state['step'] > 1:
        self.decisions.append(self.decision)
        self.actions.append(self.next_action)
        self.states.append(self.df)
        self.q_values.append(self.priorities[self.decision])
        self.max_q_values.append(max(self.priorities))
        
        rew = self.qs.reward(e, self.events, self.last_action, self.next_action, self.dist_to_coin, self.last_to_coin, self.game_state["self"][3], self.df1)
        self.rewards.append(rew)
        
        self.last_to_coin = self.dist_to_coin
        self.last_action = self.next_action
    self.logger.info("rewarded")
    return None


def q_fuction(state, q_values):
    """
    Calculation a value for each possible movement 
    """
    return state 

def update_q(q_old, r, mfr, lr = 0.1, df = 0.9): 
    """
    Updating the q-table according to Bolzmann Formula
    """
    return (1 - lr) * np.array(q_old) + lr * (r + np.array(df) * np.array(mfr))

def end_of_episode(self):
    self.logger.info("exploration")
    self.logger.info(self.qs.exploration_rate)
    self.logger.info("end of episode")
    if self.qs.exploration_rate > self.qs.min_exploration_rate:
        self.qs.exploration_rate = self.qs.exploration_rate - self.qs.exploration_decay
    for i in range(6):
	# n - step q learning algorithm. exeption for n = 1 for performance reasons
        n = 1
        if n == 1:
            mask = (np.array(self.decisions) == i)
            values = np.array(self.q_params[i])
            q_old = np.array(self.q_values)[mask]
            q_max = np.array(self.max_q_values)[mask]
            rew = np.array(self.rewards)[mask]
            states = np.array(self.states)[mask][:-1].tolist()

            up = update_q(q_old[:-1], rew[:-1], q_max[1:], self.qs.learning_rate, self.qs.discount_factor).tolist()
            self.logger.info("updated values")
            self.logger.info(up)

            for q in up:
                self.q_new.append(q)

            for s in states:
                self.state_new.append(s)
            for n in range(len(up)):
                self.decision_new.append(i)
            totmask = (np.array(self.decision_new) == i)

            if len(totmask) > 0:
                newval = lsq_linear(np.array(self.state_new)[totmask], np.array(self.q_new)[totmask], (-30, 30))['x']
                self.logger.info(newval)
                norman = np.linalg.norm(np.dot(np.array(self.state_new)[totmask], np.array(newval)) - np.array(self.q_new)[totmask]) 
                self.logger.info(norman)
                self.q_params[i] = newval

        else:
            mask = (np.array(self.decisions) == i)
            values = np.array(self.q_params[i])

            #n-step Q-Learning
            if n < len(mask) + 1:
                n = len(mask)

            states = np.array(self.states)[mask][:-n].tolist()
            q_old = np.array(self.q_values)[mask][:-n]
            rew = np.array(self.rewards)[mask][:-n]

            q_max = np.array(self.max_q_values)[mask]
            q_max1 = []
            for m in range(len(q_max) - n):
                m += 1
                q_int = 0
                for l in range(n):
                    q_int += self.qs.learning_rate ** l * q_max[m + l]
                    q_max1.append(q_int)
            q_max = q_max1

            up = update_q(q_old, rew, q_max, self.qs.learning_rate, self.qs.discount_factor).tolist()
            self.logger.info("updated values")
            self.logger.info(up)

            for q in up:
                self.q_new.append(q)

            for s in states:
                self.state_new.append(s)
            for n in range(len(up)):
                self.decision_new.append(i)
            totmask = (np.array(self.decision_new) == i)

            if len(totmask) > 0:
                self.logger.info("new values and dist to fitting data")
                newval, res, rnk, s = lsq_linear(np.array(self.state_new)[totmask], np.array(self.q_new)[totmask], (-30, 30))
                self.logger.info(newval)
                norman = np.linalg.norm(np.dot(np.array(self.state_new)[totmask], np.array(newval)) - np.array(self.q_new)[totmask]) 
                self.logger.info(norman)
                self.q_params[i] = newval

        if self.training:
            cut = self.qs.cut
            if len(self.q_new) > cut:
                self.q_new = self.q_new[-cut:]
                self.state_new = self.state_new[-cut:]
                self.decision_new = self.decision_new[-cut:]
            self.qs.save_q(self.task, self.q_params, self.q_new, self.state_new, self.decision_new)
            self.logger.info("updated parameters saved")
    self.logger.info("end")
    return None