import numpy as np
import pandas as pd
import pickle
import random 

from data_handler import DataHandler
def setup(self):
    self.logger.info("Initialisation")
    # initialize model
    infile = open("randomforestregressor.pickle",'rb')
    model_dict =  pickle.load(infile)
    self.model = model_dict["model"]
    self.features = model_dict["features"]
    self.target = model_dict["target"]
    # self.players = 4 #len(self.game_state["others"])
    self.all_actions = ["LEFT", "UP", "RIGHT", "DOWN", "BOMB", "WAIT"]
    self.dh = DataHandler()
    self.logger.info("Features: %s" % str(self.features))
    self.logger.info("Target: %s" % str(self.target))
    # parameter to save the game and the decisions
    self.df = pd.DataFrame()
    self.debug = False



def act(self):

    # get current state
    df = self.dh.game_state_to_df(self.game_state)
    # get available actions
    possible_actions = np.array(self.dh.get_available_actions(
        self.game_state["self"],
        self.game_state["arena"]
    ))
    df = self.dh.actions_to_df(df, possible_actions, all_actions=self.all_actions)
    X = df[self.features]
    # predict winning probability
    y_predict = self.model.predict_proba(X)
    # print(y_predict)
    # TODO: get all actions, which are available
    self.logger.info("Available actions: %s" % str(possible_actions))
    self.logger.info("prediction: %s" % str(y_predict))
    # decide if we are in a high risk situation: 
    # just go into high risk situation if probability to die is higher then 0.3
    # for a move unequal to lay a bomb (index=1)
    if max(np.delete(y_predict[:, 0], 1)) >0.5: 
        self.logger.info("Agent is in high risk situation")
        self.next_action = possible_actions[np.argmin(y_predict[:,0])]
    # low-risk situation
    else: 
        # choose index of actions where -1 is unlikely
        low_risk_actions = np.where(y_predict[:,0]<0.3)
        # if multiple  actions have a higher probability then a specific threshold then choose randomly
        hp_actions =  np.where(y_predict[low_risk_actions[0]][:,2]>=0.97) # high-probability actions
        if len(hp_actions[0]) > 1: 
            action_index = hp_actions[0][np.random.randint(len(hp_actions[0]))]
            action_index = low_risk_actions[0][action_index]
            self.logger.info("Choose randomly between actions %s " %  (str(possible_actions[low_risk_actions[0][hp_actions]])))
        else: 
            # choose action with highest probability for 1
            action_index = low_risk_actions[0][np.argmax(y_predict[low_risk_actions][:,2])]
        self.next_action = possible_actions[action_index]

    # save data for debugging
    if self.debug:
        X.loc[:, "NEXT_EVENT"] = self.next_action
        X.loc[:, "X"], X.loc[:, "Y"], X.loc[:, "NAME"], X.loc[:, "BOMB"] = self.game_state["self"] 
        X.loc[:, "STEP"] = self.game_state["step"]
        self.df = pd.concat([self.df, X], axis=0, sort=False, ignore_index=True)
        self.df.to_csv("dataForPrediction.csv")
        with open("gamestate.pickle", 'wb') as f:
            pickle.dump(self.game_state, f)

def reward_update(self):
    pass

def end_of_episode(self):
    pass
