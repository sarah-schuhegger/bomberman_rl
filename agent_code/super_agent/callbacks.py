import numpy as np
import pandas as pd
import pickle
import random 
from data_handler import DataHandler
from settings_for_ml import prediction_dict
from settings import settings


def setup(self):
    self.logger.info("Initialisation")

    # test if we should solve task 1 or task 2/3: 
    if settings["crate_density"] == 0: 
        task = "task1"
    else: 
        task = "task2"
    # define prediction, all actions, columns to drop, ... 
    self.p_name = prediction_dict[task]["prediction_name"]
    self.all_actions = prediction_dict[task]["all_actions"]
    self.to_drop = prediction_dict[task]["to_drop"]
    self.to_drop_actions = prediction_dict[task]["to_drop_actions"]
    self.random_threshold = prediction_dict[task]["random_threshold"]
    self.logger.info("name of used prediction: %s" % self.p_name)

    # load the prediction
    infile_p = open("agent_code/super_agent/%s" % self.p_name,'rb')
    self.model_dict_p = pickle.load(infile_p)

    # initialize class datahandler
    self.logger.info( "features: %s " %  self.model_dict_p["features"])
    self.dh = DataHandler()

    # parameter to save the game and the decisions
    self.debug = False
    self.previous_event = "WAIT"
    self.df = pd.DataFrame()




def act(self):
    # TODO: Implement random actions
    # get current state
    df = self.dh.game_state_to_df(self.game_state)
    # get available actions
    available_actions = np.array(self.dh.get_available_actions(
        self.game_state["self"],
        self.game_state["arena"].transpose(),
        self.to_drop_actions
    ))
    # add next actions and precious action to dataframe
    df = self.dh.actions_to_df(df, available_actions, all_actions=self.all_actions)
    df = self.dh.previous_event_to_df(df, self.previous_event, all_actions=self.all_actions)
    self.logger.info("Available actions: %s" % str(available_actions))

    # choose next action form prediction with the highest future rewards
    next_action, y_predict = choose_highest_future_rewards(self, df, self.model_dict_p, available_actions, self.random_threshold )
    self.previous_event = next_action
    self.next_action = next_action
    self.logger.info("Next action: %s" % next_action)

    # set self.debug=True to track the data and the predictions of the agent
    if self.debug:
        df.loc[:, "NEXT_EVENT"] = self.next_action
        df.loc[:, "AVAILABLE_ACTIONS"] = str(available_actions)
        df.loc[:, "PROBABILITIES"] = str(y_predict)
        df.loc[:, "X"], df.loc[:, "Y"], df.loc[:, "NAME"], df.loc[:, "BOMB"] = self.game_state["self"] 
        df.loc[:, "STEP"] = self.game_state["step"]
        self.df = pd.concat([df, self.df], axis=0, sort=False, ignore_index=True)
        self.df.to_csv("dataForPrediction.csv")
        with open("gamestate.pickle", 'wb') as f:
            pickle.dump(self.game_state, f)




def choose_highest_future_rewards(self, df, model_dict, available_actions, random_threshold, randomness=True): 
    features = model_dict["features"]
    model = model_dict["model"]
    X = df[features]

    # predict future rewards
    y_predict = model.predict(X)
    self.logger.info("prediction: %s" % str(y_predict))
    actions_index = [np.argmax(y_predict)]
    max_value = y_predict[actions_index[0]]
    # if randomness = True, select all indices of actions which differ not more 
    # then random_threshold from the maximum value. 
    if randomness:
        for i, val in enumerate(y_predict): 
            if i != actions_index[0] and val >= max_value - random_threshold: 
                actions_index.append(i)
        # choose a random action from actions_index: 
        final_action_index = actions_index[np.random.randint(len(actions_index))]
    else: 
        final_action_index = actions_index[0]
    next_action = available_actions[final_action_index]
    return next_action, y_predict


def reward_update(self):
    pass

def end_of_episode(self):
    pass

