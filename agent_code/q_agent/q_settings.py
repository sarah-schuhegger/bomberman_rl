import itertools as it
import pandas as pd
import numpy as np
import pickle 


class Settings():

    def __init__(self, task, table = False):
        # going rom randomly exploring the environment to exploting it
        self.exploration_rate = 1
        self.min_exploration_rate = 0.01
        self.exploration_decay = 0.01

        # values considering the q-learning algorithm as well as handeling different training Methods for different tasks
        self.task = task
        self.training = table
        self.learning_rate = 0.1
        self.discount_factor = 0.9

        # actions and observations
        self.action_space = ['RIGHT', 'LEFT', 'UP', 'DOWN', 'WAIT'] # excluded bomb for now
        self.state_space_task = self.state_space(task)

        # either initialising a q-table or loading it from previous training runs
        if self.training:
            self.actions = np.zeros(5)
            self.q_table = {obs: self.actions for obs in self.state_space_task}
            with open(r"agent_code/q_agent/q_tables/q_task" + str(self.task) + ".pickle", "wb") as file:
                pickle.dump(self.q_table, file, fix_imports = False)
        else:
            with open(r"agent_code/q_agent/q_tables/q_task" + str(self.task) + ".pickle", "rb") as file:
                self.q_table = pickle.load(file)

    def episode():
        """
        epsilon greedy arguments
        """
        if exploration_rate < min_exploration_rate:
            exploration_rate -= exploration_decay_rate

    def state_space(self, task):
        """
        introduce different state spaces for each task to make testing easier.
        Also i think this will make it easier to identify wich permutations are usefull vs wich are not
        """
        with open(r"dataForPrediction.csv", "rb") as file:
            data = pd.read_csv(file)
        available_obs = data.keys()
        comfort = [obs for obs in available_obs if "COMFORT" in obs]
        dof0 = ["DOF_DOWN", "DOF_LEFT", "DOF_UP", "DOF_RIGHT"]
        dof = [a + b for a in dof0 for b in dof0 if a != b]
        coin0 = ['COIN_POTENTIAL_DOWN', 'COIN_POTENTIAL_LEFT', 'COIN_POTENTIAL_RIGHT', 'COIN_POTENTIAL_UP']
        coin = [a + b for a in coin0 for b in coin0 if a != b]
        if task == 1:
            acts = it.product(self.action_space, coin, dof)
            stracts = ["".join(state) for state in acts]
            return stracts
        elif task == 2:
            pass
        elif task == 3:
            pass

    def save_q(self, q, task):
        with open(r"agent_code/q_agent/q_tables/q_task" + str(task) + ".pickle", "wb") as file:
            pickle.dump(q, file, fix_imports = False)