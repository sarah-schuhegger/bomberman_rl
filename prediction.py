import numpy as np
import pandas as pd
import seaborn as sn
import pickle
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_absolute_error, mean_squared_error, confusion_matrix
from data_handler import DataHandler


class Prediction():
    def __init__(self, df, classifier_name="RandomForestRegressor", target="WINNER", to_drop=["X", "Y", "NAME", "CURRENT_SCORE", "NEXT_SCORE"], max_depth=20):
        assert target in df.columns, "target %s is not in columns of df" % target
        self.classifier_name = classifier_name
        self.df = df
        self.target = target
        self.to_drop = to_drop
        self.dh = DataHandler()

        if self.classifier_name == "RandomForestRegressor":
            self.model = self.dfToRf(rf_type="Regressor", max_depth=max_depth)
        if self.classifier_name == "RandomForestClassifier":
            self.model = self.dfToRf(rf_type="Classifier", max_depth=max_depth)

    def dfToRf(self, rf_type="Regressor", max_depth=30):
        """
        get Random Forest Classifier from 
        """
        # get X and y
        X = self.df.drop([self.target], axis=1)
        self.y = self.df[self.target]
        self.X = X.drop(self.to_drop, axis=1)

        self.features = self.X.columns
        # split train and test data
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
            self.X, self.y, test_size=0.33, random_state=42)
        if rf_type == "Regressor":
            rf = RandomForestRegressor(n_estimators=100, max_depth=max_depth)
        elif rf_type == "Classifier":
            rf = RandomForestClassifier(n_estimators=100, max_depth=max_depth)
        rf.fit(self.X_train, self.y_train)
        return rf

    def hyper_parameter_tuning(self):
        # random search and cross validation
        pass

    def save_model(self, name="randomforestregressor.pickle"):
        d = {
            "y_true_train": self.y_train,
            "y_predict_train": self.model.predict(self.X_train),
            "y_true_test": self.y_test,
            "y_predict_test": self.model.predict(self.X_test),
            # retrain model on whole data set
            "model": self.model.fit(self.X, self.y),
            "features": self.features,
            "target": self.target,

        }
        print(">> save model in file %s" % name)
        pickle.dump(d, open(name, "wb"))

    def evaluate_model(self):
        """
        evaluate model for train and test data
        """
        y_train_predict = self.model.predict(self.X_train)
        y_test_predict = self.model.predict(self.X_test)
        for name, y_predict, y_true in zip(["train", "test"], [y_train_predict, y_test_predict], [self.y_train, self.y_test]):
            print("mean_absolute_error %s-error: %f" %
                  (name, mean_absolute_error(y_predict, y_true)))
            print("mean_squared_error %s-error: %f" %
                  (name, mean_squared_error(y_predict, y_true)))
            if self.classifier_name == "RandomForestRegressor":
                # true vs. predicted plot 
                plt.scatter(y_true, y_predict)
                plt.plot(y_true, y_true, label="perfect fit", color="black")
                plt.xlabel("true")
                plt.ylabel("predicted")
                plt.legend(loc=0)
                plt.show()
                # residuals plot
                plt.scatter(y_true, y_true-y_predict)
                plt.plot(y_true, np.linspace(0, 0, len(y_true)),
                         label="perfect fit", color="black")
                plt.plot(y_true, y_true, color="red",
                         label="prediction of zero")
                plt.xlabel("true")
                plt.ylabel("true-predicted")
                plt.legend(loc=0)
                plt.show()
            if self.classifier_name == "RandomForestClassifier":
                print("Confusion matrix for %s-data" % name)
                labels = np.unique(y_true)
                # calculate confusion matrix
                cm = confusion_matrix(y_true, y_predict, labels=labels)
                # normalize confusion matrix
                cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
                # .style.background_gradient()
                df = pd.DataFrame(data=cm, columns=labels, index=labels)
                sn.heatmap(df, annot=True, cmap="YlGnBu")
                plt.xlabel("Predicted Label")
                plt.ylabel("True Label")
                plt.show()
        # plot feature importance
        feat_importances = pd.Series(
            self.model.feature_importances_, index=self.features)
        feat_importances.sort_values(ascending=False).plot(kind='bar')
        plt.title("Feature importance %s" % self.target)
        plt.show()


def evaluate_pickle(pickle_file_path, name_to_save=False):
    """
    function to evaluate a model out of the pickel file. 
    """
    with open('%s' % pickle_file_path, 'rb') as f:
        d = pickle.load(f)

    y_predict_test, y_true_test = d["y_predict_test"], d["y_true_test"]
    y_predict_train, y_true_train = d["y_predict_train"], d["y_true_train"]
    print(">> lenght of data: ", len(y_predict_test) + len(y_predict_train))
    features = d["features"]
    target = d["target"]
    model = d["model"]
    for name, y_predict, y_true in zip(["train", "test"], [y_predict_train, y_predict_test], [y_true_train, y_true_test]):
        print("mean_absolute_error %s-error: %f" %
              (name, mean_absolute_error(y_predict, y_true)))
        print("mean_squared_error %s-error: %f" %
              (name, mean_squared_error(y_predict, y_true)))
        # plot true values against predicted values
        plt.figure(figsize=(10, 7))
        plt.scatter(y_true, y_predict)
        plt.plot(y_true, y_true, label="perfect fit", color="black")
        plt.xlabel("true")
        plt.ylabel("predicted")
        plt.legend(loc=0)
        if name_to_save:
            plt.savefig("%s_1.png" % name_to_save)
        plt.show()

        # residuals plot
        plt.figure(figsize=(10, 7))
        plt.scatter(y_true, y_true-y_predict)
        plt.plot(y_true, np.linspace(0, 0, len(y_true)),
                 label="perfect fit", color="black")
        plt.plot(y_true, y_true, color="red", label="prediction of zero")
        plt.xlabel("true")
        plt.ylabel("true-predicted")
        plt.legend(loc=0)
        if name_to_save:
            plt.savefig("%s_2.png" % name_to_save)
        plt.show()

    # plot feature importance
    plt.figure(figsize=(10, 7))
    feat_importances = pd.Series(model.feature_importances_, index=features)
    feat_importances.sort_values(ascending=False).plot(kind='bar')
    plt.title("Feature importance %s" % target)
    plt.gcf().subplots_adjust(bottom=0.15)
    if name_to_save:
        plt.savefig("%s_3.png" % name_to_save)
    plt.show()


if __name__ == "__main__":
    df = pd.read_csv("data.csv", index_col=0)
    p = Prediction(df, classifier_name="RandomForestClassifier")
    p.save_model()
