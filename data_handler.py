import pickle
import numpy as np
import pandas as pd
import copy
import re
from potential import Potential
from settings_for_ml import map_weights, comfort_mapping
from settings import e
from data_multiplier import DataMultiplier
from add_target import AddTarget

at = AddTarget()


class DataHandler():
    """
    class to handle stored pickle data from training data of games.
    """

    def __init__(self):
        self.matrix19x19 = 0
        # coin - potential matrix
        self.matrix_cpotential = 0
        # player - potential
        self.matrix_ppotential = 0
        self.move_map = {
            e.MOVED_LEFT: "LEFT",
            e.MOVED_RIGHT: "RIGHT",
            e.MOVED_UP: "UP",
            e.MOVED_DOWN: "DOWN",
            e.WAITED: "WAIT",
            e.BOMB_DROPPED: "BOMB",
            e.INVALID_ACTION: "WAIT",  # TODO: What is a invalid action?
        }
        self.p = Potential()

    def arena_append_frame(self, arena):
        """
        convert 17x17 arena to 19x19 arena.
        append -1 to edges.
        """
        matrix = np.full((19, 19), -1)
        matrix[1:18, 1:18] = arena[:, :, 0]
        matrix = np.atleast_3d(matrix)
        for i in range(1, arena.shape[2]):
            temp = np.full((19, 19), -1)
            temp[1:18, 1:18] = arena[:, :, i]
            matrix = np.dstack((matrix, temp))
        return matrix

    def coins_to_arena(self, coins, arena):
        """
        Input: arena 17x17xsteps
        Output: arena 17x17xsteps with coin positions
        """
        for step, coins_in_step in enumerate(coins):
            for coin in coins_in_step:
                arena[coin[1], coin[0], step] = map_weights["coin"]
        return arena

    def explosion_to_arena(self, explosion, arena):
        """
        Function for game_state
        Input: explosion matrix 2d, 
        arena matrix 3d 17x17x1

        Output: 
        arena with saved explosion
        """
        #ex = state["explosions"]
        pos = np.where((explosion == 1) | (explosion == 2))
        arena[pos[0], pos[1], 0] = map_weights["explosion"]
        return arena

    def manipulate_bombs(self, bombs):
        """
        One bomb is explodes at time step t = -1 and t = -2, 
        therefore we have to append the explode information to the bomb-list.
        Input: bombs, e.g. [[],[(1, 14, 4)],[(1, 14, 3)],[(1, 14, 2)],[(1, 14, 1)],[(1, 14, 0)]]
        Output: bombs, e.g.[[],[(1, 14, 4)],[(1, 14, 3)],[(1, 14, 2)],[(1, 14, 1)],[(1, 14, 0)], [(1, 14, -1)], [(1, 14, -2)]]
        TODO: extension to last two steps, that they get recognized as well.
        """
        for i, step in enumerate(bombs[5:-2]):
            i = i+5
            for bomb in step:
                if bomb[2] == 0 and i < len(bombs)-2:
                    bombs[i+1].append((bomb[0], bomb[1], -1))
                    bombs[i+2].append((bomb[0], bomb[1], -2))
        # add information for last 2 steps as well
        if len(bombs) > 5:
            if len(bombs[-3]) != 0:
                for bomb in bombs[-3]:
                    if bomb[2] == -1:
                        # add t = -2  bomb entry to the penultimate entry
                        bombs[-2].append((bomb[0], bomb[1], -2))
                    elif bomb[2] == 0:
                        # add t = -2  bomb entry to the penultimate entry
                        # add t = -1  bomb entry to the last bombs entry
                        bombs[-2].append((bomb[0], bomb[1], -1))
                        bombs[-1].append((bomb[0], bomb[1], -2))

                    elif bomb[2] == 1:
                        # add t = -1 to the las bombs entry
                        bombs[-1].append((bomb[0], bomb[1], -1))
        return bombs

    def bombs_to_arena(self, bombs, arena):
        """
        Function for pickle file.
        add bombs with different states of explosion to arena
        """
        # add explode information t = -1 and t = -2
        bombs = self.manipulate_bombs(bombs)
        for step, bomb_in_step in enumerate(bombs):
            for bomb in bomb_in_step:
                # define x, y, t
                xbomb, ybomb, timebomb = bomb[0], bomb[1], bomb[2]

                arena[ybomb, xbomb, step] = map_weights["bomb"]
                arena = self.explosion_area(
                    arena, step, xbomb, ybomb, timebomb)
        return arena

    def explosion_area(self, arena, step, x, y, t):
        """
        Function for pickle file.
        adding the bombs explosion area
        """
        x_shape, y_shape, _ = arena.shape
        explosion_value = map_weights["explosion_count_down"][t]
        # go over all directions LEFT, UP, RIGHT, DOWN
        for _, weight_x, weight_y in zip(["LEFT", "UP", "RIGHT", "DOWN"], [-1, 0, 1, 0], [0, -1, 0, 1]):
            for i in range(1, 4):
                xe = x + weight_x*i
                ye = y + weight_y*i
                # test if we are at the end of the field
                if xe < 0 or xe > x_shape-1 or ye < 0 or ye > y_shape-1:
                    break
                tile = copy.deepcopy(arena[ye, xe, step])
                # test if explosion hits a stone
                if tile == map_weights["stone"] or (tile in map_weights["explosion_count_down"].values()
                                                    and tile < explosion_value):
                    break
                # test until bomb does not explode, if there is a crate
                if tile == map_weights["crate"] and t >= 0:
                    continue
                else:
                    arena[ye][xe][step] = explosion_value
        return arena

    def player_to_arena(self, agent_array, arena):
        """
        Input: 
        list of arrays with xy coordinates of players.
        example: [d["%s_xy" % a] for a in d["agents"]]
        arena: 17x17 arena 
        """
        for xy in agent_array:
            arena[xy[:, 1], xy[:, 0], np.arange(len(xy))] = -2
        return arena

    def get_surrounding_fields5x5(self, x, y, step, arena19x19):
        """
        get surrounding 5 x 5 - field of agent and positionx,y 
        """
        y, x = y+1, x+1
        return arena19x19[y-2:y+3, x-2:x+3, step]

    def get_surrounding_fields3x3(self, x, y, step, arena19x19):
        y, x, = y+1, x+1
        return arena19x19[y-1:y+2, x-1:x+2, step]

    def manipulate_arena(self, coins, agent_array, bombs, arena):
        """
        Add all important informations to arena: 
        - coins
        - bombs 
        - other players
        - explosions
        Output: 19x19 arena with coin, player and explosion information
        """
        # add coin-information
        arena = self.coins_to_arena(coins, arena)
        # add players to arena
        arena = self.player_to_arena(agent_array, arena)
        # add bombs and explosion to arena
        arena = self.bombs_to_arena(bombs, arena)
        # arena = self.explosion_area(explosion, arena)
        arena = self.arena_append_frame(arena)
        return arena

    def columns_for_matrix5x5(self):
        # define column names for 5x5 matirx
        name5x5 = np.array(["%s%s" % (str(i), str(j))
                            for i in np.arange(-2, 3) for j in np.arange(-2, 3)])
        name5x5 = ["field_%s" % i for i in name5x5]
        return name5x5

    def matrix5x5todf(self, matrix):
        """
        define name for columns and save matrix as dataframe
        """
        # define column names
        name5x5 = self.columns_for_matrix5x5()
        # save to df
        if len(matrix.shape) == 1:
            return pd.DataFrame(data=matrix.reshape(1, -1), columns=name5x5)
        else:
            return pd.DataFrame(data=matrix, columns=name5x5)

    def xy_to_array5x5(self, xy, arena19x19):
        """
        save sourrounding 5x5 matrix for each position as array 
        and append arrays for each step.
        """
        iterate_xy = iter(enumerate(xy))
        # initialize first array with i = 0 and first position
        i, pos = next(iterate_xy)
        matrix = self.get_surrounding_fields5x5(
            pos[0], pos[1], i, arena19x19).reshape(25)
        for i, pos in iterate_xy:
            # get sourrounding 5x5 field
            fields = self.get_surrounding_fields5x5(
                pos[0], pos[1], i, arena19x19)
            # append 5x5 row to array
            matrix = np.vstack((matrix, fields.reshape(25)))
        return matrix

    def df_further_information_for_game_history(self, agent, xy, scores, next_event, active_agents, last_event):
        """
        create dataframe with further information about game history of a specific agent.
        """
        df_further_information = pd.DataFrame({"NAME": agent,
                                               "X": xy[:, 0],
                                               "Y": xy[:, 1],
                                               "STEP": np.arange(xy.shape[0]),
                                               "CURRENT_SCORE": scores,
                                               "NEXT_SCORE": scores[1:]+[0],
                                               "NEXT_EVENT": next_event + [e.WAITED],
                                               "PREVIOUS_EVENT": [e.WAITED] + next_event,
                                               "ACTIVE_AGENTS": active_agents[0:len(xy)],
                                               "MAX_STEP": (xy.shape[0]-1),
                                               "LAST_EVENT": last_event,
                                               })

        return df_further_information

    def df_further_information_for_game_state(self, agent_name, xy, step, active_agents):
        """
        get further information for game_state as input.
        active_agents: len(others)
        """
        df_further_information = pd.DataFrame({"NAME": [agent_name],
                                               "X": [xy[0]],
                                               "Y": [xy[1]],
                                               "STEP": [step],
                                               "ACTIVE_AGENTS": [active_agents],
                                               })
        return df_further_information

    def df_agent(self,
                 arena19x19,
                 agent,
                 xy,
                 coins,
                 bombs,
                 others,
                 active_agents,
                 scores=False,
                 next_event=False,
                 agent_name=False,
                 step=False,
                 game_state=False,
                 last_event=False):
        """
        get dataframe for one agent
        Implement: 
        - degrees of freedom
        - danger-level
        """
        # matrix = self.xy_to_array5x5(xy, arena19x19)
        # self.danger_level(x, y, step, arena19x19)
        #df = self.matrix5x5todf(matrix)
        df = pd.DataFrame()
        # create temporary other player potential
        #player_potential = p.object_to_potential(xy[0], xy[1], self.matrix_ppotential)
        for i, xy_value in enumerate(xy):
            # subtract position of player from overall potential
            temporar_player_potential = self.p.object_to_potential(
                xy_value[0], xy_value[1], self.matrix_ppotential[:, :, i], subtract=True)
            # get danger level
            cl = self.danger_level(xy_value[0], xy_value[1], i, arena19x19)
            step_dict = {"DANGER_LEVEL_%s" %
                         key: [cl[key]] for key in cl.keys()}
            # det contact to explosion (0/1)
            #
            # get degrees of freedom
            dof = self.degrees_of_freedom(
                xy_value[0], xy_value[1], i, arena19x19)
            step_dict.update(
                {"DOF_%s" % key: [dof[key]] for key in dof.keys()})
            # get coin-potential
            cp = self.p.get_potential_features(xy_value[0],
                                               xy_value[1],
                                               self.matrix_cpotential[:, :, i])
            step_dict.update(
                {"COIN_POTENTIAL_%s" % key: [cp[key]] for key in cp.keys()}
            )
            # add direction of max coin-potential
            cp_bool = self.p.get_potential_maxfeatures(xy_value[0],
                                                       xy_value[1],
                                                       self.matrix_cpotential[:, :, i])
            step_dict.update(
                {"MAX_COIN_POTENTIAL_%s" %
                    key: [cp_bool[key]] for key in cp_bool.keys()}
            )
            # add player potential
            pp = self.p.get_potential_features(xy_value[0],
                                               xy_value[1],
                                               temporar_player_potential)
            step_dict.update(
                {"OTHER_PLAYER_POTENTIAL_%s" %
                    key: [pp[key]] for key in pp.keys()}
            )
            # add direction for max player potential
            pp_bool = self.p.get_potential_maxfeatures(xy_value[0],
                                                       xy_value[1],
                                                       temporar_player_potential)
            step_dict.update(
                {"MAX_OTHER_PLAYER_POTENTIAL_%s" %
                    key: [pp_bool[key]] for key in pp_bool.keys()}
            )
            # add contact_to_explosion
            cte = self.contact_to_explosion(
                xy_value[0], xy_value[1], i, arena19x19)
            step_dict.update(
                {"CONTACT_TO_EXPLOSION_%s" %
                    key: [cte[key]] for key in cte.keys()}
            )

            # add Creates to destroy
            ctd = self.bombs_and_crates(
                xy_value[0], xy_value[1], i, arena19x19)
            step_dict.update(
                {"CRATES_TO_DESTROY_%s" %
                    key: [ctd[key]] for key in ctd.keys()}
            )
            # add dead end 0/1 feature
            de = self.dead_end(xy_value[0], xy_value[1], i, arena19x19)
            step_dict.update(
                {"DEAD_END_%s" % key: [de[key]] for key in de.keys()}
            )
            # add information of x and y position normed to the center
            relative_xy = {"RELATIVE_X":  round(np.abs((xy_value[0]-8.5)/7.5), 2),
                           "RELATIVE_Y": round(np.abs((xy_value[1]-8.5)/7.5), 2)}
            step_dict.update(relative_xy)
            # add closest distances to dataframe

            matrix_bombs = np.array([[v[0], v[1]]
                                     for v in bombs[i] if len(v) >= 2])
            matrix_coins = np.array([[v[0], v[1]]
                                     for v in coins[i] if len(v) >= 2])
            matrix_players = np.array([[v[0], v[1]]
                                       for v in others[i] if len(v) >= 2])
            # print(matrix_bombs, matrix_coins, matrix_players)
            closest_to = {"CLOSEST_DISTANCE_COIN": self.closest_distance_to(xy_value[0], xy_value[1], matrix_coins),
                          "CLOSEST_DISTANCE_BOMB": self.closest_distance_to(xy_value[0], xy_value[1], matrix_bombs),
                          "CLOSEST_DISTANCE_OTHERS": self.closest_distance_to(xy_value[0], xy_value[1], matrix_players)}
            step_dict.update(closest_to)
            df = pd.concat([df, pd.DataFrame(step_dict)],
                           axis=0, ignore_index=True, sort=True)

        if game_state:
            df_further_information = self.df_further_information_for_game_state(
                agent_name[0], xy[0], step, active_agents)
        else:
            df_further_information = self.df_further_information_for_game_history(
                agent, xy, scores, next_event, active_agents, last_event)
        df = pd.concat([df_further_information, df], sort=True, axis=1)
        return df

    def pickle_to_df(self, filename):
        """
        convert pickle file to dataframe
        """
        # read pickle file and write to dictionary
        infile = open(filename, 'rb')
        d = pickle.load(infile)
        # bring coins list to the same length as bombs and arena
        coins = d["coins"]
        agents = d["agents"]
        if "winner" not in d.keys():
            d["winner"] = agents[np.argmax(
                [max(d["%s_score" % agent]) for agent in agents])]
        # manipulate arena, add coins, bombs, ...
        self.arena19x19 = self.manipulate_arena(
            coins, [d["%s_xy" % a] for a in agents], d["bombs"], d["arena"])
        # create a coin-potential map
        self.matrix_cpotential = self.p.get_potential_3d_matrix(coins)
        self.matrix_ppotential = self.p.list_of_xys_to_3d_matrix(
            [d["%s_xy" % a] for a in d["agents"]], len(d["bombs"]))
        # define dataframe
        df = pd.DataFrame()

        # add for each agent the dataframe
        for agent in agents:
            # get other player list of tuple for agent:
            len_game = len(d["bombs"])
            other_agents = ["%s_xy" %
                            a for a in d["agents"] if not a.startswith(agent)]
            other_agents_pos = [[(d[a][i][0], d[a][i][1]) for a in other_agents if i < len(
                d[a])] for i in range(len_game)]
            df_temp = self.df_agent(self.arena19x19,
                                    agent,
                                    d["%s_xy" % agent],
                                    d["coins"],
                                    d["bombs"],
                                    other_agents_pos,
                                    #[d["%s_xy" % a] for a in d["agents"] if a != agent],
                                    active_agents=[(len(i)+1)
                                                   for i in other_agents_pos],
                                    # d["active_agents"][0:len(
                                    #    d["%s_xy" % agent])],
                                    scores=d["%s_score" % agent],
                                    next_event=d["%s_next_event" % agent],
                                    last_event=d["%s_last_event" % agent]
                                    )

            df = pd.concat([df, df_temp], axis=0,
                           sort=True, ignore_index=True)
        df["WINNER"] = (df["NAME"] == d["winner"])*1

        df = self.encode_events(df, prefix="NEXT_EVENT")
        df = self.encode_events(df, prefix="PREVIOUS_EVENT")

        # add improvement target for prediction
        # df = at.add_improvement_target_to_df(df, t=5, max_step=400)

        # add survive next steps - target
        # df = at.survive_next_steps(df, steps=4)
        # add future rewards as target
        # df = at.future_rewards(df, discount_factor=0.795)
        df = at.add_task1_target(df)
        df = at.add_task2_target(df)
        # delete df lines where curren score = 0 and next_score = 1 for step 0 because of wrong initalization of scores.
        df.drop(df[(df.STEP == 0) & (df.CURRENT_SCORE == 0) & (
            df.NEXT_SCORE == 1)].index, axis=0, inplace=True)
        return df

    def game_state_to_df(self, game_state):
        # TODO: score, players
        xy = game_state["self"][0:2]
        others = [np.array([[i[0], i[1]]]) for i in game_state["others"]]
        agent_name = game_state["self"][2]
        arena = np.atleast_3d(game_state["arena"].transpose())
        # manipulate arena and append frame
        self.arena19x19 = self.manipulate_arena([game_state["coins"]], others, [
            game_state["bombs"]], arena)
        # set coin potential matrix
        self.matrix_cpotential = self.p.get_potential_3d_matrix(
            [game_state["coins"]])
        # set player potential matrix
        self.matrix_ppotential = self.p.list_of_xys_to_3d_matrix(
            others + [np.array([[xy[0], xy[1]]])], 1)
        # append further information
        df = self.df_agent(self.arena19x19,
                           [agent_name],
                           [xy],
                           [game_state["coins"]],
                           [game_state["bombs"]],
                           [game_state["others"]],
                           agent_name=agent_name,
                           # active_agents calculated correctly?
                           active_agents=len(others)+1,
                           step=game_state["step"],
                           game_state=True)
        return df

    def encode_events(self, df, prefix="NEXT_EVENT"):
        # prefix is NEXT_EVENT or PREVIOUS_EVENT
        assert prefix in list(df.columns), "%s is not in columns." % prefix

        df[prefix] = df[prefix].map(self.move_map)
        df = pd.get_dummies(
            df, columns=[prefix], prefix=prefix, prefix_sep="_")
        # test if a few events do not appear
        all_events = ["%s_BOMB" % prefix, "%s_LEFT" % prefix, "%s_RIGHT" %
                      prefix, "%s_UP" % prefix, "%s_DOWN" % prefix, "%s_WAIT" % prefix]
        for i in all_events:
            if not i in df.columns:
                df.loc[:, i] = 0
        df.drop([i for i in df.columns if i.startswith(prefix)
                 and not i in all_events], axis=1, inplace=True)
        return df

    def actions_to_df(self, df, actions, all_actions=["LEFT", "UP", "RIGHT", "DOWN", "BOMB", "WAIT"]):
        # get df for possible actions
        columns = ["NEXT_EVENT_%s" % a for a in actions]
        temp = pd.DataFrame(
            np.eye(len(actions), dtype=int),
            columns=columns
        )
        # set all not available actions to zero
        for a in all_actions:
            if a not in actions:
                temp["NEXT_EVENT_%s" % a] = 0
        # duplicate rows of df
        if len(columns) > 1:
            df = df.append([df]*(len(columns)-1), ignore_index=True)
        # append action-df with df
        return pd.concat([df, temp], axis=1, sort=True)

    def previous_event_to_df(self, df, previous_action, all_actions=["LEFT", "UP", "RIGHT", "DOWN", "BOMB", "WAIT"]):
        # get df for possible actions
        df_length = len(df)
        df_temp = pd.DataFrame({"PREVIOUS_EVENT_%s" % a: [(
            previous_action == a)*1] * df_length for a in all_actions})
        return pd.concat([df, df_temp], axis=1, sort=True)

    def degrees_of_freedom(self, x, y, step, arena19x19):
        """
        get the degrees of freedom as dictionary
        of the sourrounding fields, with 19x19arena
        """
        matrix = self.get_surrounding_fields5x5(x, y, step, arena19x19)
        # convert matrix 0-> agent can go there 1 -> barrier
        matrix = np.where((matrix == 1) | (matrix == -1), 1, 0)
        # define degrees of freedom
        dof = {"LEFT": 0, "UP": 0, "RIGHT": 0, "DOWN": 0}
        center_x, center_y = 2, 2
        for direction, x, y in zip(dof.keys(), [-1, 0, 1, 0], [0, -1, 0, 1]):
            # if the degrees of freedom is not 0, calculated dof again
            if matrix[center_y+y, center_x+x] != 1:
                # sum over all sourrounding fields to get degrees of freedom
                degrees_of_freedom = 4 - (matrix[center_y+y+1, center_x+x] + matrix[center_y+y, center_x+x+1] +
                                          matrix[center_y+y-1, center_x+x] + matrix[center_y+y, center_x+x-1])
                dof[direction] = degrees_of_freedom
        return dof

    def danger_level(self, x, y, step, arena19x19):
        """
        get the danger level for the sourrounding fields of the agent and the 
        field of the agent.
        Other Players, Bombs, ...: decrease danger level
        """
        # get surrounding 5x5 matrix
        matrix = self.get_surrounding_fields5x5(x, y, step, arena19x19)
        # set all fields where we have stones or crates to 0
        matrix = np.where((matrix == map_weights["crate"]) | (matrix == map_weights["stone"])
                          | (matrix == map_weights["nothing"]) | (matrix == map_weights["coin"]), 0, matrix)
        # define danger level
        dl = {"LEFT": 0, "UP": 0, "RIGHT": 0, "DOWN": 0, "CENTER": 0}
        center_x, center_y = 2, 2
        if matrix[center_y, center_x] == map_weights["other_player"]:
            # overwrite own postition palyer information with 0, because the danger level of the agent himtself is neutral
            matrix[center_y, center_x] = 0
        for direction, x, y in zip(dl.keys(), [-1, 0, 1, 0, 0], [0, -1, 0, 1, 0]):
            # get the mean danger-level of surrounding field, weight not center values with 1/8 and center with 1/2
            center = matrix[center_y + y, center_x + x]
            if center in [map_weights["stone"], map_weights["other_player"], map_weights["crate"]]:
                # weight stones, crates and player higher negative, that agent know'that he can't go there
                center = comfort_mapping[center]

            dl[direction] = (1/2)*center\
                + (1/8)*(matrix[center_y+y+1, center_x+x] + matrix[center_y+y, center_x+x+1]
                         + matrix[center_y+y-1, center_x+x] + matrix[center_y+y, center_x+x-1])
        return dl

    def contact_to_explosion(self, x, y, step, arena19x19):
        """
        Get binary feature of information, if you are in an explosion area 
        or if you would run into an explosion field
        """
        cte = {"CENTER": 0, "UP": 0, "RIGHT": 0, "DOWN": 0, "LEFT": 0}
        matrix = self.get_surrounding_fields3x3(x, y, step, arena19x19)
        x_center, y_center = 1, 1
        for pos, dx, dy in zip(cte.keys(), [0, 0, 1, 0, -1], [0, -1, 0, 1, 0]):
            val = matrix[y_center+dy][x_center+dx]
            cte[pos] = 1*((val == map_weights["explosion_count_down"][0])
                          or (val == map_weights["explosion_count_down"][-1]))
            # just give direction of bomb, if it is in explosion area as well
            if (cte["CENTER"] == 1) and (val == map_weights["bomb"]):
                cte[pos] = 1
        return cte

    def closest_distance_to(self, x, y, matrix):
        """
        Get the closest distance to the next bomb, player, coin. 
        Use the L1-norm. This gives you the number of steps to the next item.
        matrix shape: (*, 2)
        """
        max_distance = 14 + 14
        # if matrix.shape[0] is zero -> return infinity
        # in our situtaiton is infitity near to 15 + 15
        if matrix.shape[0] == 0:
            return 0  # nearliy infinity for our setup
        # calculate difference between player and objects
        diff = np.abs(np.full((len(matrix), 2), np.array([x, y])) - matrix)
        # sum up x and y values
        diff_sum = np.sum(diff, axis=1)
        # calculate the min distance and norm it
        # 1 -> near to next object 0 -> no object available  or realy far away
        return round(1 - (min(diff_sum)/max_distance), 3)

    def bombs_and_crates(self, x, y, step, arena):
        """
        Returns a dictionary with 5 entrys (current, up, right, down, left) 
        that contains how many crates can be destroyed
        if a bomb is placed at the current position or one adjacent field.
        A negative value indicates, that the direction can not be reached.
        """
        # shift x and y if input arena has shape 19 x 19
        if (arena.shape[0] == 19) and (arena.shape[1] == 19):
            x += 1
            y += 1
        bac = {"CENTER": 0, "UP": 0, "RIGHT": 0, "DOWN": 0, "LEFT": 0}
        for pos, dx, dy in zip(bac.keys(), [0, 0, 1, 0, -1], [0, -1, 0, 1, 0]):
            # check current and adjacent fileds how many crates can be blown up
            bac_count = 0
            x_ = x + dx
            y_ = y + dy
            # test if it is possible to lay a bomb at current possition

            if arena[y_, x_, step] in [map_weights["crate"], map_weights["stone"],
                                       map_weights["bomb"], map_weights["other_player"],
                                       map_weights["comfort_crate"], map_weights["comfort_stone"],
                                       map_weights["comfort_other_player"]] and pos != "CENTER":
                continue

            for weight_x, weight_y in zip([-1, 0, 1, 0], [0, -1, 0, 1]):
                # run down the explosion area of the possible bomb and count the crates
                for i in range(1, 4):
                    xe = x_ + weight_x*i
                    ye = y_ + weight_y*i
                    tile = copy.deepcopy(arena[ye, xe, step])
                    if tile == map_weights["stone"]:
                        break
                    elif tile == map_weights["crate"]:
                        bac_count += 1
            bac[pos] = bac_count
        return bac

    def dead_end(self, x, y, step, arena):
        # test if we have to shift x and y by 1
        if arena.shape[0] == 19 and arena.shape[1] == 19:
            x += 1
            y += 1

        de = {"UP": 0, "RIGHT": 0, "DOWN": 0, "LEFT": 0}
        dead_end_matrix = np.where(np.isin(arena, [map_weights["stone"],
                                                   map_weights["crate"],
                                                   map_weights["bomb"],
                                                   map_weights["other_player"]]), 1, 0)[:, :, step]

        for pos, dx, dy in zip(de.keys(), [0, 1, 0, -1], [-1, 0, 1, 0]):
            if dead_end_matrix[y + dy, x + dx] == 1:
                de[pos] = 0
                continue
            sde = self.short_dead_end(x, y, dx, dy, pos, dead_end_matrix)
            if sde in [0, 1]:
                de[pos] = sde
            else:
                i = 1
                while sde not in [0, 1]:
                    sde = self.short_dead_end(
                        x + dx*i, y + dy*i, dx, dy, pos, dead_end_matrix)
                    i += 1
                de[pos] = sde

        return de

    def rotate_vector(self, vec, rotate, rotation_matrix=np.array([[0, -1], [1, 0]])):
        """
        default: 90-degree rotation matrix
        """
        return np.matmul(np.linalg.matrix_power(rotation_matrix, rotate), vec)

    def short_dead_end(self, x, y, dx, dy, pos, dead_end_matrix):
        if dead_end_matrix[y + dy, x + dx] == 1:
            return 0
        rotations = {"UP": 0, "RIGHT": 1, "DOWN": 2, "LEFT": 3}
        rotate = rotations[pos]
        edge1 = self.rotate_vector(np.array([-1, -1]), rotate)
        edge2 = self.rotate_vector(np.array([0, -2]), rotate)
        edge3 = self.rotate_vector(np.array([+1, -1]), rotate)
        # sum all edges together. 1: you can't go there. 0: you can go there
        sum_edges = dead_end_matrix[edge1[1] + y, edge1[0] + x] \
            + dead_end_matrix[edge2[1] + y, edge2[0] + x] \
            + dead_end_matrix[edge3[1] + y, edge3[0] + x]
        # we have a dead_end with dof = 1
        if sum_edges == 3:
            return 1
        # we do not have a dead_end
        if sum_edges <= 1:
            return 0
        # we may have a dead end
        if sum_edges == 2:
            return -1

    def get_available_actions(self, game_state_self, game_state_arena, to_drop=[]):
        """
        Function for game state to get all possible actions.
        """
        actions = []
        if "WAIT" not in to_drop:
            actions.append("WAIT")

        x = game_state_self[0]
        y = game_state_self[1]
        # check if bomb option is possible
        if game_state_self[3] == 1 and not "BOMB" in to_drop:
            actions.append("BOMB")
        # test if stone or crate is in direction
        for direction, dx, dy in zip(["LEFT", "UP", "RIGHT", "DOWN"], [-1, 0, 1, 0], [0, -1, 0, 1]):
            if not game_state_arena[y+dy][x+dx] in [map_weights["crate"],
                                                    map_weights["stone"],
                                                    map_weights["bomb"],
                                                    map_weights["other_player"]]:
                actions.append(direction)
        return actions


if __name__ == "__main__":
    # initialize classes
    dh = DataHandler()
    dm = DataMultiplier()
    at = AddTarget()
    #df = dh.pickle_to_df(filename)

    filename = "training_data_1_16155-25_shape.csv"
    df = pd.read_csv(filename, index_col=0)
    # test if improvement target is in data included
    if len([i for i in df.columns if i.startswith("IMPROVEMENT_TARGET")]) == 0:
        df = at.add_improvement_target_to_df(df)
    df = at.add_task1_target(df)

    # eightfold data with rotation and reflection
    df = dm.eightfold_data(df)
    # save data for prediction
    df.to_csv("data.csv")
