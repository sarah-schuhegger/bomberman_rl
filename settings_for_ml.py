
from settings import e
map_weights = {"stone": -1,
               "crate": 1,
               "coin": 8,
               "nothing": 0,
               "other_player": -2, 
               "explosion_count_down": {4: -4, 3: -6, 2: -8, 1: -10, 0:-12, -1: -24, -2: -24.05}, # for t = -1, -2, the bomb explodes
               "bomb": -26, 
               "comfort_crate": -4, # TODO: Evtl. aus Code raus nehmen -> nochmal überlegen
               "comfort_stone": -4,
               "comfort_other_player": -4,
               }
comfort_mapping = {map_weights["other_player"]: map_weights["comfort_other_player"], 
                   map_weights["stone"]: map_weights["comfort_stone"],
                   map_weights["crate"]: map_weights["comfort_crate"],
                   }
penalization_weights = {
    e.KILLED_SELF: -0.4,
    e.GOT_KILLED: -0.3,
    e.SURVIVED_ROUND: 0,
}
penalization_thinking_to_long = -1
# reference map for potential of one coin nameing convention: shape-shape-multiplier
POTENTIAL =  "coin_potential_59-59-1.npy" 
step_punishment = 0.05
max_depth=30


prediction_dict = { 
        "task1": 
                {"target": "TARGET_TASK1", 
                "cut_off": "CUT_OFF_TARGET_TASK1",
                "to_drop": ["WINNER", 'CUT_OFF_FUTURE_REWARDS0.795', 
                        'CUT_OFF_SURVIVE4', "MAX_STEP", "NEXT_SCORE", 
                        "CURRENT_SCORE", "FUTURE_REWORDS_DISCOUNT0.795", ""
                        "X", "Y", "LAST_EVENT", "SURVIVE_4STEPS", "CUT_OFF_TARGET_TASK1",
                        "ACTIVE_AGENTS", "CLOSEST_DISTANCE_BOMB", "CLOSEST_DISTANCE_OTHERS",
                        "CRATES_TO_DESTROY_RIGHT", "CRATES_TO_DESTROY_UP", "CRATES_TO_DESTROY_DOWN", 
                        "CRATES_TO_DESTROY_LEFT", "CRATES_TO_DESTROY_CENTER", 
                        "CUT_OFF_FUTURE_REWARDS0.795", "CUT_OFF_SURVIVE4",
                        "MAX_OTHER_PLAYER_POTENTIAL_DOWN", "MAX_OTHER_PLAYER_POTENTIAL_UP",
                        "MAX_OTHER_PLAYER_POTENTIAL_LEFT", "MAX_OTHER_PLAYER_POTENTIAL_RIGHT",
                        "OTHER_PLAYER_POTENTIAL_DOWN", "OTHER_PLAYER_POTENTIAL_UP", 
                        "OTHER_PLAYER_POTENTIAL_RIGHT", "OTHER_PLAYER_POTENTIAL_LEFT",
                        "DEAD_END_DOWN", "DEAD_END_LEFT", "DEAD_END_RIGHT", "DEAD_END_UP",
                        "DANGER_LEVEL_CENTER", "DANGER_LEVEL_UP", "DANGER_LEVEL_DOWN",
                        "DANGER_LEVEL_RIGHT", "DANGER_LEVEL_LEFT", "NEXT_EVENT_BOMB", "NEXT_EVENT_WAIT", "PREVIOUS_EVENT_WAIT",
                        "PREVIOUS_EVENT_BOMB", "PREVIOUS_EVENT_WAIT", 
                        "TARGET_TASK2", "CUT_OFF_TARGET_TASK2",
                        "CONTACT_TO_EXPLOSION_CENTER", "CONTACT_TO_EXPLOSION_UP", "CONTACT_TO_EXPLOSION_RIGHT",
                        "CONTACT_TO_EXPLOSION_DOWN", "CONTACT_TO_EXPLOSION_LEFT"
                        # "COIN_POTENTIAL_LEFT", "COIN_POTENTIAL_RIGHT",
                        # "COIN_POTENTIAL_UP", "COIN_POTENTIAL_DOWN"
                        ],
                "all_actions": ["LEFT", "UP", "RIGHT", "DOWN"],
                "to_drop_actions": ["WAIT", "BOMB"],
                "index": ["GAME", "NAME", "STEP"],
                "drop_wait_bomb": True,
                "random_threshold": 0.002,
        
                "base_data_name": "base_data_task1_si_r.csv",
                "base_prediction_data_name": "p_task1_si_r.csv",
                "base_prediciton_data_path": "agent_code/super_agent/",
                "base_prediction_name": "p_task1_si_r.pickle",
                "base_prediction_path": "agent_code/super_agent/",
                "prediction_name": "p_task1_si_r.pickle",
                "final_prediction": "2019-03-22_17-16_p_task1_si_r_final.pickle",
                },
        "task2": 
                {"target": "TARGET_TASK2", # TODO 
                "cut_off": "CUT_OFF_TARGET_TASK2", # TODO - Future reward, discount factor evtl. ändern 
                "to_drop": ["WINNER", 'CUT_OFF_FUTURE_REWARDS0.795', 
                        "TARGET_TASK1", "CUT_OFF_TARGET_TASK2",
                        'CUT_OFF_SURVIVE4', "MAX_STEP", "NEXT_SCORE", 
                        "CURRENT_SCORE", "FUTURE_REWORDS_DISCOUNT0.795", ""
                        "X", "Y", "LAST_EVENT", "SURVIVE_4STEPS", "CUT_OFF_TARGET_TASK1",
                        "ACTIVE_AGENTS", "CLOSEST_DISTANCE_OTHERS",
                        "CUT_OFF_FUTURE_REWARDS0.795", "CUT_OFF_SURVIVE4",
                        "MAX_OTHER_PLAYER_POTENTIAL_DOWN", "MAX_OTHER_PLAYER_POTENTIAL_UP", # No other agents available
                        "MAX_OTHER_PLAYER_POTENTIAL_LEFT", "MAX_OTHER_PLAYER_POTENTIAL_RIGHT",
                        "OTHER_PLAYER_POTENTIAL_DOWN", "OTHER_PLAYER_POTENTIAL_UP", 
                        "OTHER_PLAYER_POTENTIAL_RIGHT", "OTHER_PLAYER_POTENTIAL_LEFT",
                        ],
                "all_actions": ["LEFT", "UP", "RIGHT", "DOWN", "WAIT", "BOMB"],
                "index": ["GAME", "NAME", "STEP"],
                "drop_wait_bomb": False,
                "to_drop_actions": [],
                "random_threshold": 0.02,
                "base_data_name": "base_data_task2_si.csv",
                "base_prediction_data_name": "2019-03-23_16-39_p_task2_si.csv", #p_task2_si.csv",
                "base_prediciton_data_path": "agent_code/super_agent/",
                "base_prediction_name": "p_task2_si.pickle",
                "base_prediction_path": "agent_code/super_agent/",
                "prediction_name": "2019-03-23_19-27_p_task2_si.pickle",#"p_task2_si.pickle",
                }
}
