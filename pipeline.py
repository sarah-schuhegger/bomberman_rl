import numpy as np
import pandas as pd
import pickle
import os
import matplotlib.pyplot as plt
from data_handler import DataHandler
from add_target import AddTarget
from data_merger import DataMerger
from prediction import Prediction
from settings import settings as s
from main import main
from settings_for_ml import prediction_dict, max_depth

# initialize task 1 as default 
task1_to_drop = prediction_dict["task1"]["to_drop"]
target = prediction_dict["task1"]["target"]
cut_off = prediction_dict["task1"]["cut_off"]
base_data_name = prediction_dict["task1"]["base_data_name"]
prediciton_path = prediction_dict["task1"]["base_prediction_path"]
index = prediction_dict["task1"]["index"]
prediction_name = prediction_dict["task1"]["base_prediction_name"]
drop_wait_bomb = prediction_dict["task1"]["drop_wait_bomb"]


def create_base_prediction(file_path_list,
                           base_data_name=base_data_name,
                           target=target,
                           cut_off=cut_off,
                           to_drop=task1_to_drop,
                           index=index,
                           prediction_path=prediciton_path,
                           prediction_name=prediction_name,
                           drop_wait_bomb=drop_wait_bomb):
    # create base data
    dm = DataMerger()
    dir_name = dir_name = file_path_list[0].split("/")[0] + "/"
    base_data_path = dir_name + base_data_name
    if not os.path.exists("%s/%s" % (dir_name, base_data_name)):
        dm.create_base_data_from_files(
            file_path_list, base_data_path=base_data_path, save=True, drop_wait_bomb=drop_wait_bomb)

    # prepare base data
    df = pd.read_csv(base_data_path, index_col=0)
    to_drop_base = []
    # print(df.columns)
    for i in to_drop:
        if i in df.columns:
            to_drop_base.append(i)
        else:
            print("Can't drop %s because it is not in the df." % i)
    df_p = dm.prepare_p_data(
        target,
        cut_off,
        to_drop=to_drop_base,
        base_data_path=base_data_path,
        name=prediction_path + prediction_name.replace("pickle", "csv"),
        prediction_id=False,
        return_df=True)

    # create base prediction
    p = Prediction(df_p, classifier_name="RandomForestRegressor",
                   target=target, to_drop=index, max_depth=max_depth)
    p.save_model(name=prediction_path +
                 prediction_name.replace("csv", "pickle"))
    return p


def create_base_data_task1(dir_name, iterations=5):
    assert s["gui"] == False, "Gui is not false"
    assert s["crate_density"] == 0, "Crate density is not zero"
    assert s["max_steps"] == 400, "Max steps is not 400"
    for _ in range(iterations):
        main(dir_name=dir_name, simple_agent_count=1,
             random_agent_count=0, super_agent_count=0)


def dir_to_file_path_list(dir_name):
    fpl = ["%s/%s" % (dir_name, i)
           for i in os.listdir(dir_name) if i.endswith("pickle")]
    return fpl


if __name__ == "__main__":

    file_path_list = dir_to_file_path_list("training_data_task1")

    # print(file_path_list)
    p = create_base_prediction(file_path_list)
