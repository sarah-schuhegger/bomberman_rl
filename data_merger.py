import pickle
import os
import numpy as np
import pandas as pd
import copy
import matplotlib.pyplot as plt
from time import time
import re
from settings_for_ml import map_weights, comfort_mapping, POTENTIAL
from settings import e
from data_handler import DataHandler
from data_multiplier import DataMultiplier
from tqdm import tqdm

import logging
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s:%(name)s:%(levelname)s: %(message)s')
logger = logging.getLogger('std')


dh = DataHandler()
dm = DataMultiplier()


class DataMerger():
    """
    p1: catoegorical positive future rewards - prediction
    p2: continuos future rewards (without zeros) - prediction
    p3: survive in next steps - prediction
    """

    def __init__(self):
        self.base_data_name = "base_data.csv"
        self.base_data_path = ""
        self.to_drop = {"FUTURE_REWARDS":
                        ["GAME", "WINNER", "SURVIVE_4STEPS", 'CUT_OFF_FUTURE_REWARDS0.795',
                         'CUT_OFF_SURVIVE4', "NAME", "MAX_STEP", "NEXT_SCORE", "CURRENT_SCORE", "X", "Y", "LAST_EVENT",
                         "TARGET_TASK1", "CUT_OFF_TASK1"],
                        "SURVIVE": ["GAME", "WINNER", 'CUT_OFF_FUTURE_REWARDS0.795', 'CUT_OFF_SURVIVE4',
                                    "NAME", "MAX_STEP", "NEXT_SCORE", "CURRENT_SCORE", "FUTURE_REWORDS_DISCOUNT0.795", "X", "Y", "LAST_EVENT",
                                    "TARGET_TASK1", "CUT_OFF_TASK1"]}
        self.pred_data_names = {
            "p1": "p1_categorical_positive_future_rewards_data",
            "p2": "p2_continuous_future_rewards_data",
            "p3": "p3_survive_data"
        }
        self.save_in = "prediction_data/"  # "../prediction_data/"

    def get_name(self, prediciton_id, dir_name):
        return self.pred_data_names[prediciton_id] + "_" + dir_name + ".csv"

    def prepare_p_data(self,
                       target,
                       cut_off,
                       to_drop,
                       name=False,
                       df=False,
                       base_data_path=False,
                       return_df=False,
                       save=True,
                       prediction_id=False):
        dir_name = False
        if base_data_path:
            dir_name = base_data_path.split(
                "/")[-1].replace("_%s" % self.base_data_name, "")

        # 1. set negative values to zero and 2. drop cut off
        # if base_data_path is available:
        df = self.prepare_base_data(base_data_path=base_data_path,
                                    df=df,
                                    target=target,
                                    cut_off=cut_off,
                                    set_to_zero=False)

        # 3., 4. & 5. eighfold, drop and save data
        df = self.eightfold_drop_and_save_data(df,
                                               prediction_id,
                                               to_drop,
                                               dir_name=dir_name,
                                               name=name,
                                               return_df=return_df,
                                               save=save
                                               )
        if return_df:
            return df

    def prepare_p1_data(self, base_data_path,
                        target="FUTURE_REWORDS_DISCOUNT0.795",
                        cut_off="CUT_OFF_FUTURE_REWARDS0.795", name=False, return_df=False):
        """
        prediciton: are future rewards positive or not
        """
        dir_name = base_data_path.split(
            "/")[-1].replace("_%s" % self.base_data_name, "")
        # 1. set negative values to zero and 2. drop cut off
        df = self.prepare_base_data(base_data_path=base_data_path,
                                    target=target,
                                    cut_off=cut_off,
                                    set_to_zero=True)

        # define dataframe of two classes
        # 0 of random forest agent should not be taken -> to confusing -> not injective data
        df0 = df[(df[target] == 0) & (df.NAME.isin(
            [i for i in np.unique(df.NAME) if not i.startswith("random")]))]
        df1 = df[(df[target] > 0)]

        # 3. concat and balance data
        entries = [len(df0), (len(df1))]
        min_entry = min(entries)

        df_pfr = pd.concat([df0.sample(min_entry), df1.sample(
            min_entry)], axis=0, sort=False, ignore_index=True)
        df_pfr.loc[df_pfr[target] > 0, target] = 1
        assert min_entry * \
            2 == len(df_pfr), "Length of dataframe does not allign to 2*min_entry"
        print(">> shape of data after balancing ", df_pfr.shape)
        # 4., 5. & 6. eighfold, drop and save data
        self.eightfold_drop_and_save_data(
            df_pfr,  "p1", self.to_drop["FUTURE_REWARDS"], dir_name, name=name)

    def prepare_p2_data(self, base_data_path,
                        target="FUTURE_REWORDS_DISCOUNT0.795",
                        cut_off="CUT_OFF_FUTURE_REWARDS0.795",
                        name=False,
                        return_df=False):
        # prepare data for the p2-prediction 
        df = self.prepare_p_data(self,
                                 base_data_path,
                                 target,
                                 cut_off,
                                 name,
                                 self.to_drop["FUTURE_REWARDS"],
                                 return_df=return_df,
                                 prediction_id="p2",
                                 )
        if return_df:
            return df

    def prepare_p3_data(self, base_data_path,
                        target="SURVIVE_4STEPS",
                        cut_off="CUT_OFF_SURVIVE4",
                        name=False,
                        return_df=False):
        """
        prepare the data for the p3-prediction
        """
        dir_name = base_data_path.split(
            "/")[-1].replace("_%s" % self.base_data_name, "")

        # 1. get base data and cut off unwanted columns
        df = self.prepare_base_data(
            base_data_path=base_data_path, target=target, cut_off=cut_off, set_to_zero=False)

        # 2. balance data
        df0 = df[df["SURVIVE_4STEPS"] == 0]
        df1 = df[df["SURVIVE_4STEPS"] == 1]
        entries = [len(df0), len(df)]
        min_entry = min(entries)
        df_s = pd.concat([df0.sample(min_entry), df1.sample(min_entry)])
        print(">> shape of data after balancing ", df_s.shape)

        self.eightfold_drop_and_save_data(df_s,
                                          "p3",
                                          self.to_drop["SURVIVE"],
                                          dir_name,
                                          name=name,
                                          return_df=return_df)
        if return_df:
            return df

    def prepare_base_data(self, df=False, base_data_path=False, 
                          target="FUTURE_REWORDS_DISCOUNT0.795", 
                          cut_off="CUT_OFF_FUTURE_REWARDS0.795", set_to_zero=False):
        """
        prepare the base data for a aspecific target. 
        - drop cut_off for the target
        """
        if base_data_path:
            df = pd.read_csv(base_data_path, index_col=0)
        print(">> shape of base data ", df.shape)
        if set_to_zero:
            # 1. set all future rewads smaller then 0 to to
            df.loc[df[target] < 0, target] = 0
        # 2. delete all future rewards with cut_off = 1
        df.drop(df[df[cut_off] == 1].index, inplace=True)
        print(">> shape of base data after cut-off ", df.shape)

        return df

    def eightfold_drop_and_save_data(self, df, prediciton_id, to_drop, dir_name=False, name=False, return_df=False, save=True):

        # 5. drop columns
        # drop just columns which are as well in the dataframe
        to_drop = list(set(df.columns) & set(to_drop))
        df = df.drop(to_drop, axis=1)

        # 5. eightfold data
        df_e = dm.eightfold_data(df)
        assert len(
            df)*8 == len(df_e), "Length of dataframe does not allign to 8*2*min_entry "
        print(">> shape of data after eightfold data and drop columns", df_e.shape)

        # 6. save data
        if not name and save == True:
            name = self.save_in + self.get_name(prediciton_id, dir_name)
        if save:
            df_e.to_csv(name)
            print(">> save data to: %s" % name.replace("pickle", "csv"))
        if return_df:
            return df_e

    def plot_hist(self, df, target="FUTURE_REWORDS_DISCOUNT0.795", logy=False):
        # plot histogram of target variable
        df[target].hist(bins=20)
        if logy:
            plt.yscale("log")
        plt.plot()
        plt.title(target)
        plt.show()

    def create_base_data_from_files(self, file_path_list, base_data_path=True, save=True, drop_wait_bomb=False):
        # merge files from file_path_list
        t1 = time()
        logger.debug(">> Convert %d files" % len(file_path_list))
        df = self.merge_files_to_df(file_path_list)
        t2 = time()
        logger.debug(">> time to merge files: %1.2f s " % (t2-t1))
        logger.debug(">> shape of data: %s" % str(df.shape))
        # preprocess dataframe if necessary
        if drop_wait_bomb:
            if "NEXT_EVENT_WAIT" in df.columns:
                df.drop(df[df.NEXT_EVENT_WAIT == 1].index,
                        axis=0, inplace=True)
                logger.debug(">> drop waits from dataframe")
            if "NEXT_EVENT_BOMB" in df.columns:
                df.drop(df[df.NEXT_EVENT_BOMB == 1].index,
                        axis=0, inplace=True)
                logger.debug(">> drop waits from dataframe")
        # save file to base_data_path
        if save:
            logger.debug(">> save file to %s" % base_data_path)
            df.to_csv(base_data_path)
        # return df
        else:
            return df

    def merge_files_to_df(self, file_path_list):
        # merge files of file_path_list to dataframe
        logger.debug('starting merge_files_to_df')
        df = pd.DataFrame()

        df_list = []
        for i in tqdm(range(0, len(file_path_list))):
            logger.debug('dh.pickle_to_df start')
            df_temp = dh.pickle_to_df(file_path_list[i])
            # numerate games
            df_temp["GAME"] = i
            # append game to dataframe list
            df_list.append(df_temp)

        logger.debug('merge_files_to_df: pd.concat: start')
        # concatenate all dataframes
        df = pd.concat(df_list, axis=0, ignore_index=True, sort=True)
        logger.debug('merge_files_to_df: pd.concat: end')

        return df

