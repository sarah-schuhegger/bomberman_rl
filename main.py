
from time import time, sleep
import contextlib
from time import time

with contextlib.redirect_stdout(None):
    import pygame
from pygame.locals import *
import numpy as np
import multiprocessing as mp
import threading
import pickle

from environment import BombeRLeWorld, ReplayWorld 
from settings import s, settings

import logging
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s:%(name)s:%(levelname)s: %(message)s')
logger = logging.getLogger('std')


# Function to run the game logic in a separate thread
def game_logic(world, user_inputs):
    last_update = time()
    while True:
        if world.thread_alive == False:
            break
        # Game logic
        if (s.turn_based and len(user_inputs) == 0):
            sleep(0.1)
        elif (s.gui and (time()-last_update < s.update_interval)):
            sleep(s.update_interval - (time() - last_update))
        else:
            last_update = time()
            if world.running:
                try:
                    world.do_step(user_inputs.pop(0) if len(user_inputs) else 'WAIT')
                except Exception as e:
                    world.end_round()
                    raise

dir_name="test_data"
# dir_name = "training_data_task1"
def main(dir_name = dir_name, random_agent_count=0, simple_agent_count=0, super_agent_count=1, pygame_init=True):
    if pygame_init:
        pygame.init()

    # Emulate Windows process spawning behaviour under Unix (for testing)
    # mp.set_start_method('spawn')

    # Initialize environment and agents
    agents = []
    for i in range(super_agent_count): agents.append(('super_agent', False)) 
    for i in range(simple_agent_count): agents.append(('simple_agent', False)) 
    for i in range(random_agent_count): agents.append(('random_agent', False)) 

    world = BombeRLeWorld(agents)
    # world = ReplayWorld('Replay 2019-01-30 16:57:42')
    user_inputs = []

    # Start game logic thread
    t = threading.Thread(target=game_logic, args=(world, user_inputs))
    t.daemon = True
    t.start()

    # Run one or more games
    for i in range(s.n_rounds):
        if not world.running:
            world.ready_for_restart_flag.wait()
            world.ready_for_restart_flag.clear()
            world.new_round()
        
        world.reset_data()

        # First render
        if s.gui:
            world.render()
            pygame.display.flip()

        round_finished = False
        last_update = time()
        last_frame = time()
        user_inputs.clear()

        # Main game loop
        while not round_finished:
            # Grab events
            key_pressed = None
            for event in pygame.event.get():
                if event.type == QUIT:
                    world.end_round()
                    world.end()
                    return
                elif event.type == KEYDOWN:
                    key_pressed = event.key
                    if key_pressed in (K_q, K_ESCAPE):
                        world.end_round()
                    if not world.running:
                        round_finished = True
                    # Convert keyboard input into actions
                    if s.input_map.get(key_pressed):
                        if s.turn_based:
                            user_inputs.clear()
                        user_inputs.append(s.input_map.get(key_pressed))

            if not world.running and not s.gui:
                round_finished = True

            # Rendering
            if s.gui and (time()-last_frame >= 1/s.fps):
                world.render()
                pygame.display.flip()
                last_frame = time()
            else:
                sleep_time = 1/s.fps - (time() - last_frame)
                if sleep_time > 0:
                    sleep(sleep_time)
                if not s.gui:
                    last_frame = time()

        # saving training data
        agentinfo = ""
        agenttypes = {"random_agent": 0, "super_agent": 0, "simple_agent" : 0, "q_agent": 0, "user_agent": 0}
        agentabr = {"random_agent": "r", "super_agent": "su", "simple_agent": "si", "q_agent": "q", "user_agent": "u"}
        for agent in world.agents:
            for agent_type in agenttypes.keys():
                if agent.name.startswith(agent_type):
                    agenttypes[agent_type] += 1
        for agent_type in agenttypes.keys():
            agentinfo = agentinfo + str(agentabr[agent_type]) + str(agenttypes[agent_type])
        name = "time" + str(time()).replace(".", "") \
                        + "crates" + str(settings["crate_density"]).replace(".", "")  \
                        + agentinfo \
                        + "maxsteps" + str(settings["max_steps"]) \
                        + ".pickle"
        
        with open(r"%s/%s" % (dir_name, name), 'wb') as f:
                # save arena in the right format
                print("save game to: %s/%s" % (dir_name, name))

                pickle.dump(world.data, f, protocol = 2, fix_imports = False)
    logger.debug('world.end()')
    world.end()
    world.thread_alive = False
    logger.debug('t.join() enter')
    t.join()
    logger.debug('t.join() exit')


if __name__ == '__main__':
    settings["gui"] == True
    dir_name="training_data_task2_simple_random_base"
    main(dir_name="test_data", random_agent_count=0, simple_agent_count=0, super_agent_count=1, pygame_init=True)
